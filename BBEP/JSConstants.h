//
//  JSConstants.h
//  BBEP
//
//  Created by Davyd Geyl on 17/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

extern CGFloat const JSPhoneTitlebarHeight;
extern CGFloat const JSPadTitlebarHeight;

extern NSString* const JSWebsiteURL;
extern NSString* const JSNTIWebsiteURL;

@interface JSConstants : NSObject

+ (JSConstants*)sharedConstants;

- (CGFloat)titleBarHeightForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
- (UIColor*)barTintColor;
- (UIColor*)toolbarTintColor;

@property (nonatomic, readonly) CGColorSpaceRef colorSpaceDeviceRGB;

@end
