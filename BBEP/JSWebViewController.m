//
//  JSWebViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 13/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSWebViewController.h"

@interface JSWebViewController () <UIWebViewDelegate>

@property (nonatomic, strong) UIWebView* webView;
@property (nonatomic, strong) UIActivityIndicatorView* activityIndicator;
@property (nonatomic, strong) UIBarButtonItem* backBarButtonItem;

@end

@implementation JSWebViewController

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        _webView = [UIWebView new];
        _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _webView.delegate = self;
        _webView.scalesPageToFit = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _webView.frame = self.view.bounds;
    [self.view addSubview:_webView];
}

- (void)setLinkURL:(NSURL*)linkURL
{
    _linkURL = linkURL;
    [_webView loadRequest:[NSURLRequest requestWithURL:_linkURL]];
}

- (void)goBack:(id)sender
{
    [_webView goBack];
}

- (UIBarButtonItem*)backBarButtonItem
{
    if (_backBarButtonItem == nil)
        _backBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Go Back", @"Button title") style:UIBarButtonItemStylePlain  target:self action:@selector(goBack:)];
    
    return _backBarButtonItem;
}

#pragma mark - 

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    if (_activityIndicator == nil)
    {
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    
    [_activityIndicator startAnimating];
    
    CGSize webViewBoundsSize = _webView.bounds.size;
    CGSize activityIndicatorSize = _activityIndicator.bounds.size;
    _activityIndicator.frame = CGRectMake(round((webViewBoundsSize.width - activityIndicatorSize.width) / 2.0), round((webViewBoundsSize.height - activityIndicatorSize.height) / 2.0), activityIndicatorSize.width, activityIndicatorSize.height);
    [_webView addSubview:_activityIndicator];
    
    [self performSelector:@selector(validateBackButton) withObject:nil afterDelay:1.0];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self hideActivity];
    [self validateBackButton];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self hideActivity];
}

- (void)validateBackButton
{
    if (_webView.canGoBack)
        self.navigationItem.rightBarButtonItem = self.backBarButtonItem;
    else
        self.navigationItem.rightBarButtonItem = nil;
}

- (void)hideActivity
{
    [_activityIndicator stopAnimating];
    [_activityIndicator removeFromSuperview];
}

@end
