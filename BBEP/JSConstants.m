//
//  JSConstants.m
//  BBEP
//
//  Created by Davyd Geyl on 17/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSConstants.h"

CGFloat const JSPhoneLandscapeTitleBarHeight = 32.0;
CGFloat const JSPhonePortraitTitleBarHeight = 64.0;
CGFloat const JSPadTitlebarHeight = 66.0;

NSString* const JSWebsiteURL = @"http://www.paradeofthebuddhas.org/";
NSString* const JSNTIWebsiteURL = @"http://www.nantien.edu.au/";

@interface JSConstants ()
{
    CGColorSpaceRef _colorSpaceDeviceRGB;
}

@end

@implementation JSConstants

+ (JSConstants*)sharedConstants
{
    static JSConstants* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,
                  ^
    {
        sharedInstance = [[JSConstants alloc] init];
    });
    return sharedInstance;
}

- (CGFloat)titleBarHeightForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    CGFloat result = 0;

    if ([[UIDevice currentDevice] systemVersion].floatValue >= 7.0)
    {
        if ((interfaceOrientation == UIInterfaceOrientationPortrait) || (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown))
        {
            result = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) ? JSPhonePortraitTitleBarHeight : JSPadTitlebarHeight;
        }
        else
        {
            result = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) ? JSPhoneLandscapeTitleBarHeight : JSPadTitlebarHeight;
        }
    }

    return result;
}

- (CGColorSpaceRef)colorSpaceDeviceRGB
{
    if (_colorSpaceDeviceRGB == nil)
        _colorSpaceDeviceRGB = CGColorSpaceCreateDeviceRGB();

    return _colorSpaceDeviceRGB;
}

- (UIColor*)barTintColor
{
    return [UIColor colorWithHue:63.0 / 360.0 saturation:0.06 brightness:1.0 alpha:1.0];
}

- (UIColor*)toolbarTintColor
{
    return [UIColor colorWithHue:63.0 / 360.0 saturation:0.06 brightness:0.7 alpha:1.0];
}

@end
