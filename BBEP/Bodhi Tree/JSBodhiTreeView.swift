//
//  JSBodhiTreeView.swift
//  BBEP
//
//  Created by Davyd Geyl on 22/11/2015.
//  Copyright © 2015 Jettysoft Pty Ltd. All rights reserved.
//

import UIKit

class JSBodhiTreeView: UIView {

    var leafButtons: [UIButton] = [UIButton]()
    var leafImages: [UIImageView] = [UIImageView]()
    var highlightTimer: Timer?
    var highlightCount: Float = 0
    var currentButton: UIButton? {
        willSet {
            currentButton?.layer.shadowOpacity = 0
            currentButton?.layer.shadowRadius = 0
        }
        didSet {
            startAnimation()
        }
    }

    struct Constants {
        static let highlightMaxCount: Float = 20.0
        static let highlightTimerInterval: TimeInterval = 0.1
    }

    override func awakeFromNib() {
        for subview in subviews {
            if let button = subview as? UIButton, button.tag >= 0 {
                leafButtons.append(button)
            }
            if let imageView = subview as? UIImageView, imageView.tag >= 0 {
                leafImages.append(imageView)
            }
        }
    }

    var numberOfLeaves: Int = 0 {
        didSet {
            updateLeaves()
        }
    }

    func updateLeaves() {
        for button in leafButtons {
            button.isHidden = button.tag > numberOfLeaves || (button.tag == 0 && numberOfLeaves > 1) || (button.tag == 1 && numberOfLeaves == 1)

            if (numberOfLeaves > 1 && button.tag == numberOfLeaves) || (numberOfLeaves == 1 && button.tag == 0) {
                currentButton = button
            }
        }
        for imageView in leafImages {
            imageView.isHidden = (imageView.tag > numberOfLeaves || (imageView.tag == 0 && numberOfLeaves > 1) || (imageView.tag == 1 && numberOfLeaves == 1)) && (imageView.tag >= 0)
        }
    }

    func startAnimation() {
        stopAnimation()
        highlightTimer = Timer.scheduledTimer(timeInterval: Constants.highlightTimerInterval, target: self, selector: #selector(JSBodhiTreeView.highlightTimerDidFire), userInfo: nil, repeats: true)
    }

    func stopAnimation() {
        highlightTimer?.invalidate()
        highlightTimer = nil
    }

    @objc
    func highlightTimerDidFire() {
        if let currentButton = currentButton {
            currentButton.layer.shadowOffset = CGSize.zero
            currentButton.layer.shadowColor = UIColor.yellow.cgColor
            currentButton.layer.shadowOpacity = 1.0
            currentButton.layer.shadowRadius = CGFloat(5.0) + CGFloat(4.0 * sin(2 * 3.14 * highlightCount / Constants.highlightMaxCount));


            highlightCount = highlightCount + 1.0
            if highlightCount > Constants.highlightMaxCount {
                highlightCount = 0
            }
        }
    }
}
