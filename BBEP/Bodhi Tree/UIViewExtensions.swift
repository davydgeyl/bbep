//
//  UIView1.swift
//  BBEP
//
//  Created by Davyd Geyl on 21/11/2015.
//  Copyright © 2015 Jettysoft Pty Ltd. All rights reserved.
//

import UIKit

extension UIView {

    func embedSubview(subview: UIView) {
        if subview.superview != self {
            subview.translatesAutoresizingMaskIntoConstraints = false
            subview.frame = bounds
            addSubview(subview)

            let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[childView]-0-|", options: NSLayoutFormatOptions.alignAllLeft, metrics: nil, views: ["childView":subview])
            addConstraints(horizontalConstraints)

            let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[childView]-0-|", options: NSLayoutFormatOptions.alignAllTop, metrics: nil, views: ["childView":subview])
            addConstraints(verticalConstraints)
        }
    }
}
