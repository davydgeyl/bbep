//
//  JSBodhiTreeViewController.swift
//  BBEP
//
//  Created by Davyd Geyl on 7/11/2015.
//  Copyright © 2015 Jettysoft Pty Ltd. All rights reserved.
//

import UIKit

class JSBodhiTreeViewController: UIViewController, JSLeafViewControllerDelegate {

    @IBOutlet private var tapHintLabel: UILabel!
    @IBOutlet private var comeBackTomorrowHintLabel: UILabel!

    private struct Constants {
        static let LeafHasBeenTappedKey = "LeafHasBeenTappedKey"
    }

    var leavesObserver: JSKeyValueObserver?

    override func viewDidLoad() {
        super.viewDidLoad()

        leavesObserver = JSKeyValueObserver(for: JSBodhiTree.sharedInstance, keyPath: "numberOfLeaves", target: self, selector: Selector(("numberOfLeavesDidChange:")))

        let addDayButton = UIBarButtonItem(title: "+", style: UIBarButtonItemStyle.plain, target: self, action: #selector(JSBodhiTreeViewController.addDay(button:)))
        let resetButton = UIBarButtonItem(title: "reset", style: UIBarButtonItemStyle.plain, target: self, action: #selector(JSBodhiTreeViewController.reset(button:)))
        self.navigationItem.setRightBarButtonItems([addDayButton, resetButton], animated: false)

        update()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        JSBodhiTree.sharedInstance.updateNumberOfLeaves()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        currentTreeView?.stopAnimation()
    }

    static let treeVariations = [1, 4, 6, 10, 15, 26, 37, 53]

    func numberOfLeavesDidChange(_ : AnyObject) {
        update()
    }

    func update() {
        currentViewName = "JSBodhiTreeView\(currentTreeVariation())"
        currentTreeView?.numberOfLeaves = JSBodhiTree.sharedInstance.numberOfLeaves
        updateHints()
    }

    func updateHints() {
        tapHintLabel?.isHidden = leafHasBeenTapped
        comeBackTomorrowHintLabel?.isHidden = !waitingForNextLeaf
    }

    var currentViewName: String? {
        didSet {
            if oldValue != currentViewName {
                if let currentViewName = currentViewName {
                    currentTreeView = viewForName(viewName: currentViewName)
                }
            }
        }
    }

    var currentTreeView: JSBodhiTreeView? {
        willSet {
            currentTreeView?.removeFromSuperview()
        }
        didSet {
            if let currentTreeView = currentTreeView {
                view.embedSubview(subview: currentTreeView)
                scaleIfNeeded()
            }
        }
    }

    private func scaleIfNeeded() {
//        var scalingFactor:CGFloat = 1.0
//        let screenHeight = UIScreen.mainScreen().bounds.size.height
//        if let currentTreeView = currentTreeView {
//
//            if screenHeight > 600 {
//                scalingFactor = 1.5
//            }
//            else if screenHeight < 500 {
//                scalingFactor = 0.9
//            }
//
//            if scalingFactor != 1.0 {
//                currentTreeView.layer.transform = CATransform
//            }
//        }
    }

    func viewForName(viewName: String) -> JSBodhiTreeView? {
        let topObjects = Bundle.main.loadNibNamed(viewName, owner: self, options: nil)
        return topObjects?.first as? JSBodhiTreeView
    }

    func currentTreeVariation() -> Int {
        let numberOfLeavesVisible = JSBodhiTree.sharedInstance.numberOfLeaves
        var previousVariation = JSBodhiTreeViewController.treeVariations.first!
        for variation in JSBodhiTreeViewController.treeVariations {
            if variation > previousVariation && numberOfLeavesVisible < variation && numberOfLeavesVisible >= previousVariation {
                return previousVariation
            }
            previousVariation = variation
        }
        return 1
    }

    //MARK - Leaves

    @IBAction func leafTapped(button: UIButton) {
        let tag = button.tag == 0 ? 1 : button.tag;
        leafViewController = JSLeafViewController()
        leafViewController.text = JSBodhiTree.sharedInstance.leafTextWithindex(index: tag - 1)
        leafViewController.view.frame = view.bounds
        leafViewController.updateViewConstraints()
        tappedLeafFrame = button.frame
        animateFromFrame(frame: button.frame)

        leafHasBeenTapped = true
    }

    var leafViewController: JSLeafViewController = JSLeafViewController()
    {
        didSet {
            leafViewController.delegate = self
        }
    }
    var zoomView: UIView?
    var tappedLeafFrame: CGRect?

    func animateFromFrame(frame: CGRect) {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        leafViewController.view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let displayView = UIImageView.init(frame: view.bounds)
        displayView.contentMode = UIViewContentMode.scaleAspectFit
        zoomView = displayView
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        displayView.image = image
        displayView.frame = frame
        view.addSubview(displayView)
        displayView.setNeedsDisplay()

        let targetFrame = view.bounds
        print("targetFrame: \(targetFrame)")

        UIView.animate(withDuration: 0.25, animations: { () -> Void in
                displayView.frame = targetFrame
            print("frame: \(displayView.frame)")
            }, completion: { [weak self] (compeleted: Bool) -> Void in
                displayView.removeFromSuperview()
                self?.addChildViewController((self?.leafViewController)!)
                if let subview = self?.leafViewController.view {
                    self?.view.embedSubview(subview: subview)
                }
            })
    }

    func animateBackToFrame(frame: CGRect) {
        leafViewController.view.removeFromSuperview()
        leafViewController.removeFromParentViewController()
        UIView.animate(withDuration: 0.25, animations: { [weak self] () -> Void in
            self?.zoomView?.frame = frame
            }, completion: { [weak self] (compeleted: Bool) -> Void in
                self?.zoomView?.removeFromSuperview()
                self?.zoomView = nil
                JSBodhiTree.sharedInstance.lastReadLeafNumber = JSBodhiTree.sharedInstance.numberOfLeaves
                self?.updateHints()
            })
    }

    func leafViewControllerDidFinish(controller: JSLeafViewController) {
        animateBackToFrame(frame: tappedLeafFrame ?? CGRect.zero)
    }

    @objc
    func addDay(button: AnyObject) {
        let number = JSBodhiTree.sharedInstance.numberOfLeaves + 1
        JSBodhiTree.sharedInstance.numberOfLeaves = number <= 52 ? number : 52
    }

    @objc
    func reset(button: AnyObject) {
        leafHasBeenTapped = false
        JSBodhiTree.sharedInstance.reset()
    }

    var leafHasBeenTapped: Bool {
        get {
            return UserDefaults.standard.bool(forKey: Constants.LeafHasBeenTappedKey) 
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.LeafHasBeenTappedKey)
            UserDefaults.standard.synchronize()
            updateHints()
        }
    }

    var waitingForNextLeaf: Bool {
        get {
            return JSBodhiTree.sharedInstance.lastReadLeafNumber == JSBodhiTree.sharedInstance.numberOfLeaves
        }
    }
}

