//
//  JSBodhiTree.swift
//  BBEP
//
//  Created by Davyd Geyl on 20/11/2015.
//  Copyright © 2015 Jettysoft Pty Ltd. All rights reserved.
//

import UIKit

class JSBodhiTree: NSObject {

    static let sharedInstance = JSBodhiTree()

    private struct Constants {
        static let MaxNumberOfLeaves = 52
        static let LeafGrowthTimeInterval = 12.0 * 3600.0 // 12 hours
        //static let LeafGrowthTimeInterval = 5.0 // 5 sec
        static let NumberOfLeavesKey = "NumberOfLeavesKey"
        static let LastUpdateTimeIntervalKey = "LastUpdateTimeIntervalKey"
        static let LastReadLeafKey = "LastReadLeafKey"
    }

    override init() {
        numberOfLeaves = 15// UserDefaults.standard.integerForKey(Constants.NumberOfLeavesKey) ?? 1
        super.init()
    }
    
    @objc
    dynamic var numberOfLeaves: Int {
        didSet {
            UserDefaults.standard.set(numberOfLeaves, forKey: Constants.NumberOfLeavesKey)
            UserDefaults.standard.synchronize()
        }
    }

    let leafTexts: [String] = {
        var result = [String]()
        if let path = Bundle.main.path(forResource: "BodhiTreeLeaves", ofType: "plist"),
            let array = NSArray(contentsOfFile: path) as? [String] {
            result = array
        }
        return result
    }()

    func leafTextWithindex(index: Int) -> String {
        var result = ""
        if index < leafTexts.count {
            result = leafTexts[index]
        }
        return result
    }

    private var lastUpdateTimeInterval: TimeInterval {
        get {
            return UserDefaults.standard.double(forKey: Constants.LastUpdateTimeIntervalKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.LastUpdateTimeIntervalKey)
            UserDefaults.standard.synchronize()
        }
    }

    var lastReadLeafNumber: Int {
        get {
            return UserDefaults.standard.integer(forKey: Constants.LastReadLeafKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.LastReadLeafKey)
            UserDefaults.standard.synchronize()
            self.lastUpdateTimeInterval = NSDate.timeIntervalSinceReferenceDate
        }
    }

    func reset() {
        numberOfLeaves = 1
        lastReadLeafNumber = 0
        lastUpdateTimeInterval = 0
    }

    func updateNumberOfLeaves() {
        let lastUpdateTimeInterval = self.lastUpdateTimeInterval
        let nowTimeInterval: TimeInterval = NSDate.timeIntervalSinceReferenceDate

        if lastReadLeafNumber == numberOfLeaves && nowTimeInterval - lastUpdateTimeInterval > Constants.LeafGrowthTimeInterval {
            self.lastUpdateTimeInterval = nowTimeInterval
            if numberOfLeaves < Constants.MaxNumberOfLeaves {
                numberOfLeaves += 1
            }
        }
    }
}
