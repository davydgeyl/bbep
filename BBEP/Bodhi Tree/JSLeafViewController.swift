//
//  JSLeafViewController.swift
//  BBEP
//
//  Created by Davyd Geyl on 21/11/2015.
//  Copyright © 2015 Jettysoft Pty Ltd. All rights reserved.
//

import UIKit

protocol JSLeafViewControllerDelegate {
    func leafViewControllerDidFinish(controller: JSLeafViewController)
}

@objc (JSLeafViewController)
class JSLeafViewController: UIViewController {

    @IBOutlet var textLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        update()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.25) { [weak self] () -> Void in
            self?.view.backgroundColor = UIColor.black.withAlphaComponent(0.35)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        view.backgroundColor = UIColor.clear
    }

    var delegate: JSLeafViewControllerDelegate?

    var text: String = "" {
        didSet {
            update()
        }
    }

    func update() {
        textLabel?.text = text
    }

    @IBAction func close(button: UIButton) {
        delegate?.leafViewControllerDidFinish(controller: self)
    }
}
