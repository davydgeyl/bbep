//
//  JSMainViewControlerNewViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 11/02/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSMenuViewController.h"

@interface JSMainViewControlerNewViewController : JSMenuViewController

@end
