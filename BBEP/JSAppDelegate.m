//
//  JSAppDelegate.m
//  BBEP
//
//  Created by Davyd Geyl on 3/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSAppDelegate.h"
#import "JSMainViewController.h"
#import "JSDharmaCardDataModel.h"

@implementation JSAppDelegate

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0)
    {
        [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                              [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0], NSForegroundColorAttributeName,
                                                              [UIColor clearColor], UITextAttributeTextShadowColor,
                                                              [UIFont fontWithName:@"Georgia" size:18.0], NSFontAttributeName,
                                                              nil]];
    }

    UILocalNotification* notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];

    [JSDharmaCardDataModel sharedDataModel].shouldShowCardUponLaunch = (notification != nil);

    return YES;
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}
#endif

- (void)application:(UIApplication*)application didReceiveLocalNotification:(UILocalNotification*)notification
{
    [JSDharmaCardDataModel sharedDataModel].shouldShowCardUponLaunch = YES;
}

@end
