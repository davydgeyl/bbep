//
//  JS356DaysWisdomViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 3/07/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JS365DaysWisdomViewController.h"
#import <MessageUI/MessageUI.h>
#import "JSConstants.h"

NSString* const JS356DaysVerseTemplate = @"verse%d_%d";
NSString* const JS356DaysVerseExtension = @"txt";

NSTimeInterval const JSOneDaySeconds = 3600 * 24;

CGFloat const JSWisdomLabelMaxWidth = 500;

@interface JS365DaysWisdomViewController () <MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) NSDate* currentDate;

@end

@implementation JS365DaysWisdomViewController

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    UIBarButtonItem* scheduleButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Schedule", @"Button title") style:UIBarButtonItemStylePlain target:self action:@selector(scheduleWords)];

    UIBarButtonItem* shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareWords)];

    self.navigationItem.rightBarButtonItems = @[shareButton];

    if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0)
    {
//        scheduleButton.tintColor = [UIColor colorWithRed:0.4 green:0.65 blue:1.0 alpha:1.0];
        shareButton.tintColor = [UIColor colorWithRed:0.4 green:0.65 blue:1.0 alpha:1.0];
    }

    self.currentDate = [NSDate dateWithTimeIntervalSinceNow:0];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    CGFloat navigationBarHeight = [[JSConstants sharedConstants] titleBarHeightForInterfaceOrientation:self.interfaceOrientation];

    CGSize boundsSize = self.view.bounds.size;

    _contentView.frame = CGRectMake(0, navigationBarHeight, boundsSize.width, boundsSize.height - navigationBarHeight);

    if (boundsSize.width >= 630)
    {
        CGFloat horizontalMargin = round((boundsSize.width - JSWisdomLabelMaxWidth) / 2.0);
        _wisdomLabel.frame = CGRectMake(horizontalMargin, _wisdomLabel.frame.origin.y, JSWisdomLabelMaxWidth, _wisdomLabel.frame.size.height);
    }
}

- (void)adjustWisdomLabelHeight
{
    CGSize size = [_wisdomLabel.text sizeWithFont:_wisdomLabel.font constrainedToSize:CGSizeMake(_wisdomLabel.frame.size.width, CGFLOAT_MAX) lineBreakMode:_wisdomLabel.lineBreakMode];

    CGRect frame = _wisdomLabel.frame;
    frame.size.height = size.height;
    _wisdomLabel.frame = frame;
}

- (void)setCurrentDate:(NSDate*)currentDate
{
    _currentDate = currentDate;
    [self showVerseForDate:_currentDate];
    [self updateDateLabelForDate:_currentDate];
}

- (IBAction)previousDay:(id)sender
{
    self.currentDate = [NSDate dateWithTimeIntervalSinceReferenceDate:[_currentDate timeIntervalSinceReferenceDate] - JSOneDaySeconds];
}

- (IBAction)nextDay:(id)sender
{
    self.currentDate = [NSDate dateWithTimeIntervalSinceReferenceDate:[_currentDate timeIntervalSinceReferenceDate] + JSOneDaySeconds];
}

- (NSString*)verseForDate:(NSDate*)date
{
    NSCalendar* calendar = [NSCalendar currentCalendar];

    NSDateComponents* components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth fromDate:date];
    NSString* fileName = [NSString stringWithFormat:JS356DaysVerseTemplate, (int)components.month, (int)components.day];

    NSError* error = nil;
    return [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:JS356DaysVerseExtension] encoding:NSUTF8StringEncoding error:&error];
}

- (void)showVerseForDate:(NSDate*)date
{
    _wisdomLabel.text = [self verseForDate:date];
    [self adjustWisdomLabelHeight];
}

- (void)updateDateLabelForDate:(NSDate*)date
{
    NSDateFormatter* formatter = [NSDateFormatter new];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MMM. d"];
    _dateLabel.text = [formatter stringFromDate:date];
}

- (IBAction)buyTheBook:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.fgp.com.my/default.aspx?portalname=www.fgp.com.my&pageid=4138&product_id=641EABBF-3F5F-4F55-88B6-30A2D54CD6BA&cat_id=F85ABCFC-F19B-4DDE-A853-57855B60AE7B%23.U6fTzgrLwlc.gmail"]];
}

- (void)scheduleWords
{
}

- (void)shareWords
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController* controller = [MFMailComposeViewController new];
        controller.mailComposeDelegate = self;
        [controller setSubject:NSLocalizedString(@"BBEP 366 Days with Wisdom", @"Mail subject")];
        
        NSError* error = nil;
        NSMutableString* bodyPage = [NSMutableString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"wisdom" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];;
        [bodyPage replaceOccurrencesOfString:@"%date" withString:_dateLabel.text options:0 range:NSMakeRange(0, bodyPage.length)];
        [bodyPage replaceOccurrencesOfString:@"%verse" withString:_wisdomLabel.text options:0 range:NSMakeRange(0, bodyPage.length)];
        [bodyPage replaceOccurrencesOfString:@"%website" withString:JSWebsiteURL options:0 range:NSMakeRange(0, bodyPage.length)];
        
        [controller setMessageBody:bodyPage isHTML:YES];

        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Default Mail Account Required", @"Alert title") message:NSLocalizedString(@"Please setup a Default Account in the Mail Settings", @"Alert message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Button") otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
