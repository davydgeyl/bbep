//
//  JS356DaysWisdomViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 3/07/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSViewController.h"

@interface JS365DaysWisdomViewController : JSViewController

@property (nonatomic, strong) IBOutlet UIView* contentView;
@property (nonatomic, strong) IBOutlet UILabel* dateLabel;
@property (nonatomic, strong) IBOutlet UILabel* wisdomLabel;

- (IBAction)previousDay:(id)sender;
- (IBAction)nextDay:(id)sender;
- (IBAction)buyTheBook:(id)sender;

@end
