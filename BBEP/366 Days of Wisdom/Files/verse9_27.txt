Whether you have a humble profession or dwell in a remote mean alley,
As long as you pursue to your vow, you live with uprightness and an indomitable spirit.
