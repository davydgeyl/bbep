By overcoming adverse circumstances, one can gain ability. By standing the tests, one can carry great responsibility.
