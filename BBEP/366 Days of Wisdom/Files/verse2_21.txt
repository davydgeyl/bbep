If you are a genius but always say "wait till tomorrow!", then you are no different from a normal person.
If you are capable but always say "let someone else do it!", then you are no different from an incapable person.
