We must constantly seek ways to create causes and conditions with others.
We must never think of how much we could gain from others.
