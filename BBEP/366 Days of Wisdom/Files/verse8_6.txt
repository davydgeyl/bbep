Good health refers mainly to the complete health of the body and mind, not the physical brute strength;
Longevity refers mainly to the continuity of wisdom, not the actual life span.
