Adverse situations are good for training our determination.
Poverty is good for the accomplishment of great personalities.
Confidence is the motivation to reach the goal.
Ambition is the compass of our life.
