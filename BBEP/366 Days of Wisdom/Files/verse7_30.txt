Ignorance and delusion originated from our grasping nature; hence to cultivate the Dharma one should begin with cultivating "no ego".
Let go of the body and mind to achieve "no ego" and the light of the true principle will shine onto your heart.
