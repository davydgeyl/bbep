Be an honest and upright gentleman who dares to speak and act.
Do not be a passive good person who is timid and confused.
