Morning is the best time of the day.
Diligence is the best practice in a lifetime.
A clear mind is the best approach to do things.
Harmony is the best principle for a happy family.
