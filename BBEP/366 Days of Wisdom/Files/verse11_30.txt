Do good deeds to leave a good name for ourselves.
Contribute to society and we have no worries of death.
Unjust acts can create slander for us.
A useless person to society is filled with fright and is as good as dead.
