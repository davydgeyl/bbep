In spring, the red flowers and green leaves complement each other.
At night, the moon and stars pass by each other in the sky shining on the vast universe.
As long as we understand tolerance and harmony,
then we shall no doubt realise that the world is actually beautiful.
