Accepting good advice can correct mistakes.
The charitable donors can cherish everything.
A gentleman accepts advice, a kind man gives charity.
