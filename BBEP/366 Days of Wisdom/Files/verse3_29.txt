True reputation is not about being praised by contemporaries, but about setting a good example for future generations.
True possession is not about owning something alone but about sharing it with others.
