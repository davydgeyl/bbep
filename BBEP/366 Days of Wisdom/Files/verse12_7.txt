Wrongs will not win reasons, reasons will not win rules and laws.
Laws will not win authorities, authorities will not win cause and effect.
