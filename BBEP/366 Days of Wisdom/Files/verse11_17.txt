Counteract wrong thoughts through positive beliefs.
Contemplate defilements through right mindfulness.
Excel in right mindfulness through monitoring all thoughts.
