Words rashly spoken to flaunt a moment's triumph cannot produce good outcome.
Words spoken with reason and deliberation are definitely tactful and harmonious.
