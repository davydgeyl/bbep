Don't be self-conceited when being praised.
Don't be resentful when being bullied.
Don't be discouraged when encountering failure.
Don't be arrogant when being successful.
