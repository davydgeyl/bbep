Life will paralyse if there is lack of self-confidence and self-esteem.
Integrity will become mean and low if we refuse to uphold virtue and practise wisdom.
