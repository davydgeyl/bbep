A truly noble person plans not only for his well being but also worries about the future of all beings.
A Less Noble practitioner is concerned over his own life, not thinking about the life of all sentient beings.
