Life should follow the practice of Buddha's Dharma.
Belief should be rational.
Handling of tasks should be harmonious.
Cultivation should be in our daily life.
