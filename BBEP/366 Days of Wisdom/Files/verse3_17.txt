Have a plan before you act.
Have a budget before you spend money.
Set a goal before you practice.
Have a hope for the future.
