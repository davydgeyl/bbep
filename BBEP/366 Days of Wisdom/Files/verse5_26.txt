Pass on life education with wisdom.
Guide juniors with experience.
Develop life with cultivation.
Probe the future with virtues.
