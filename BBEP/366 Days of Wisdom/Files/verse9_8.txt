The universe conforming to nature sprouts life.
All beings conforming to nature will grow.
An actions conforming to nature will be successfully completed.
A mind conforming to nature is benevolent and beautiful.
