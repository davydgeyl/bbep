Say more good words; always do good deeds;
Constantly introspect; always be happy.
