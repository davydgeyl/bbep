The best thing in the world is happiness.
The kindest act in the world is to form ties of good causes and conditions.
The strongest power in the world is endurance.
The most powerful determination in the world is willing to do things wholeheartedly.
