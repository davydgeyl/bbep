A mountain is high because it accumulates the earth; an ocean is vast because it accommodates all rivers.
Heaven and earth are selfless as they contain everything; Buddha nature is formless, hence it manifests everywhere.
