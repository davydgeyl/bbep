It is better to create opportunity for good causes and conditions
than to idle away waiting for them to occur.
