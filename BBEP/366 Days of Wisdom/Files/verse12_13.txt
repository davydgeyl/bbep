Believing in Buddhism is not seeking supernatural power, but to learn the Buddha's practice of both compassion and wisdom.
Believing in Buddhism is not seeking prosperity and longevity, but to comprehend the truth of the Dharma, cause, condition and retribution.
