Momentary forbearance brings permanent happiness.
Momentary grievance brings ultimate success.
