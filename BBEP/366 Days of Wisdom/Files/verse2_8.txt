To be hopeless at our own future is the most pathetic thing in life.
To have no plan in our vocation is the worst habit in life.
