Our conduct should be upright and lofty like a mountain.
Our handling of matters should be easy and smooth like flowing water.
