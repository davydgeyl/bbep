A smile is better that a bouquet of flowers.
A thought of clarity is better than a glass of clean water.
A kind word is better than a tune of music.
A word of compliment is better than a poem.
