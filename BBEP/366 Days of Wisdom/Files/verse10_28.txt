Promote harmony, care for others, appreciate opportunities, and work for the benefits of all beings.
