Diligence makes a family prosperous.
Laziness makes a family weak.
Thrift makes a family wealthy.
Being spendthrift makes a family poor.
