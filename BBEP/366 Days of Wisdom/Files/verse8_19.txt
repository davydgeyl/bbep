If you are diligent, naturally you have more time,
If you are willing to move, naturally you have a more extensive space,
If you can endure more hardship, naturally your success is greater.
