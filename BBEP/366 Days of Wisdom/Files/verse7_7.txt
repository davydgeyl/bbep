An angry thought is like wild fire, capable of burning away a whole forest of merits accumulated through many kalpas.
A single thought of dispute between self and others is enough to wash away all the amassed virtuous thoughts.
