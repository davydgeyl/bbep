We tend to lose ourselves when life goes well, hence knowing our responsibilities is the foundation of being a person.
We tend to lose faith in adverse situations, hence diligence is the foundation of dealing with general affairs.
