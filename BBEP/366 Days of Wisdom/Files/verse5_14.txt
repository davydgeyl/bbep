A mother's education to her children is a great blessing.
A wife's support to her husband is a great happiness.
