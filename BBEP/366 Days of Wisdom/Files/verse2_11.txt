Self-discovery arises after self-awareness.
Self-confidence arises after self-discovery.
Self-empowerment arises after self-confidence.
Self-independence arises after self-empowerment.
