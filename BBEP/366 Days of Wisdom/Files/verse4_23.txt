Asserting tolerance of criticism by other people is the way to behave with integrity.
Having courage to correct mistakes of oneself is the way to accomplish goals.
