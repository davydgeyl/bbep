With a mind of non-attachment, there will be no obstacles of vexations.
With a body of stability, there will be no obstacles of environment.
