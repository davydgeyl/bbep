Pay more attention to others than ourselves.
Emphasise more on giving than striving for something.
Emphasise more on thanksgiving than enmity.
Pay more attention to good virtue than capability.
