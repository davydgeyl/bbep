Busy people have no false thoughts; Idle people have no happiness,
Good people have no grudges; Bad people have no reasons.
