Harmonious emotion maintains good mood.
Peaceful heart keeps away worries.
A humble character creates good affinity.
Overcome self to be happy without worries.
