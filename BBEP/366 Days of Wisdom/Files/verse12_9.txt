"Impermanent" matters lead to the progress of civilization.
"Impermanent" life leads the way to the future of hope.
