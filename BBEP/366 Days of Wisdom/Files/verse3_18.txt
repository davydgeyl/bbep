Know a person's strength when you employ him.
Know a person's weakness when you teach him.
