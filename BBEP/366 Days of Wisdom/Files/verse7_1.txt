Contentment is like a magic stone; everything it touches changes to gold - happiness.
Gratitude is like a fairy's wand; everywhere it touches changes to Pure Land - happiness.
