As long as there are wishful thoughts, then there is no distance on this earth.
As long as there are aspiring thoughts, then there is no sadness or happiness in this human world.
