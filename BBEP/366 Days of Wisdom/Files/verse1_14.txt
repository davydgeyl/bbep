As long as the root is not rotten, a plant can blossom in wasteland.
As long as we persist with determination, we can revive a hopeless situation.
