//
//  JSSilkRoadViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 15/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSSilkRoadViewController.h"
#import "JSMapView.h"

const CGFloat JSSilkRoadViewControllerCopyrightLabelHeight = 20.0;
const CGFloat JSSilkRoadViewControllerCopyrightLabelWidth = 400.0;
const CGFloat JSSilkRoadViewControllerCopyrightLabelHorizontalPosition = 0.5;
const CGFloat JSSilkRoadViewControllerCopyrightLabelBottomMargin = 5.0;

@interface JSSilkRoadViewController ()

@property (nonatomic, strong) UILabel* copyrightLabel;

@end

@implementation JSSilkRoadViewController

- (id)init
{
    self = [super init];
    if (self)
    {
        NSArray* mapRegions = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"silkRoad" ofType:@"plist"]];
        self.mapView.mapRegions = mapRegions;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Silk Road", @"Navigation title");
    self.mapImage = [UIImage imageNamed:@"silk_road.jpg"];
    
    CGRect bounds = self.mapView.frame;
    _copyrightLabel = [[UILabel alloc] initWithFrame:CGRectMake(round(bounds.size.width * JSSilkRoadViewControllerCopyrightLabelHorizontalPosition), bounds.size.height - JSSilkRoadViewControllerCopyrightLabelHeight - JSSilkRoadViewControllerCopyrightLabelBottomMargin, JSSilkRoadViewControllerCopyrightLabelWidth, JSSilkRoadViewControllerCopyrightLabelHeight)];
    _copyrightLabel.text = @"© 2012 Nancy Cowardin / IBPS Hsilai Temple";
    [self.mapView addSubview:_copyrightLabel];
}

@end
