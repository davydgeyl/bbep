//
//  JSMainViewControlerNewViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 11/02/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSMainViewControlerNewViewController.h"
#import "JSMenuViewController.h"
#import "JSConstants.h"
#import "JSTimelineViewController.h"
#import "JSTriptychViewController.h"
#import "JSSilkRoadViewController.h"
#import "JSDharmaCardsViewController.h"
#import "JSCarriageViewController.h"
#import "JSWebViewController.h"
#import "JSDharmaCardDataModel.h"
#import "JS365DaysWisdomViewController.h"
#import "JSDashboardButton.h"
#import "JSQnACardsViewController.h"
#import "BBEP-Swift.h"

static NSString* const JSSecondaryMenuSegueIdentifier = @"SecondaryMenuSegue";

static NSString* const JSTimelineMenuIdentifier = @"Timeline";
static NSString* const JSTriptychMenuIdentifier = @"Triptych";
static NSString* const JSSilkRoadMenuIdentifier = @"SilkRoad";
static NSString* const JSLaterZhaoCarriageMenuIdentifier = @"LaterZhaoCarriage";
static NSString* const JSDharmaCardsMenuIdentifier = @"DharmaCards";
static NSString* const JS365DaysOfWisdomMenuIdentifier = @"365DaysOfWisdom";
static NSString* const JSQnACardsMenuIdentifier = @"QnACards";
static NSString* const JSBodhiTreeMenuIdentifier = @"Bodhi Tree";
static NSString* const JSAboutBBEPMenuIdentifier = @"About BBEP";
static NSString* const JSWebsiteMenuIdentifier = @"Website";
static NSString* const JSNTIMenuIdentifier = @"NTI";

NSArray* const _secondaryMenuItems = nil;

@interface JSMainViewControlerNewViewController () <JSMenuViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UIView* secondaryMenuContainerView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* secondaryMenuTopConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* secondaryMenuLeftConstraint;
@property (nonatomic, strong) NSArray* buddhaSecondaryMenuItems;
@property (nonatomic, strong) NSArray* dharmaSecondaryMenuItems;
@property (nonatomic, strong) NSArray* sanghaSecondaryMenuItems;
@property (nonatomic, strong) NSArray* currentSecondaryMenuItems;
@property (nonatomic, strong) JSMenuViewItem* currentMenuItem;
@property (nonatomic, strong) JSMenuViewController* iPadSecondaryMenuViewController;
@property (nonatomic, assign) BOOL isSecondaryMenuViewControllerPresented;
@property (nonatomic, strong) JSDashboardButton* buttonSecondaryMenuViewControllerPresentedFrom;

@end

@implementation JSMainViewControlerNewViewController

- (instancetype)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.menuItems = @[[JSMenuViewItem itemWithIdentifier:JSWebsiteMenuIdentifier title:NSLocalizedString(@"Buddha", @"Button title") imageName:@"buddha140"],
                       [JSMenuViewItem itemWithIdentifier:JSWebsiteMenuIdentifier title:NSLocalizedString(@"Dharma", @"Button title") imageName:@"dharma140"],
                       [JSMenuViewItem itemWithIdentifier:JSWebsiteMenuIdentifier title:NSLocalizedString(@"Sangha", @"Button title") imageName:@"sangha140"]];

    _buddhaSecondaryMenuItems = @[[JSMenuViewItem itemWithIdentifier:JSTimelineMenuIdentifier title:NSLocalizedString(@"Timeline", @"Button title")],
                                  [JSMenuViewItem itemWithIdentifier:JSTriptychMenuIdentifier title:NSLocalizedString(@"Triptych", @"Button title")],
                                  [JSMenuViewItem itemWithIdentifier:JSSilkRoadMenuIdentifier title:NSLocalizedString(@"Silk Road", @"Button title")],
                                  [JSMenuViewItem itemWithIdentifier:JSLaterZhaoCarriageMenuIdentifier title:NSLocalizedString(@"Later Zhao Carriage Design", @"Button title")]];

    _dharmaSecondaryMenuItems = @[[JSMenuViewItem itemWithIdentifier:JSDharmaCardsMenuIdentifier title:NSLocalizedString(@"Dharma Cards", @"Button title")],
                                  [JSMenuViewItem itemWithIdentifier:JS365DaysOfWisdomMenuIdentifier title:NSLocalizedString(@"366 Days with Wisdom", @"Button title")]
                                  ,
                                  [JSMenuViewItem itemWithIdentifier:JSQnACardsMenuIdentifier title:NSLocalizedString(@"Q&A Cards", @"Button title")],
                                  [JSMenuViewItem itemWithIdentifier:JSBodhiTreeMenuIdentifier title:NSLocalizedString(@"Bodhi Tree", @"Button title")]
    ];
    _sanghaSecondaryMenuItems = @[[JSMenuViewItem itemWithIdentifier:JSAboutBBEPMenuIdentifier title:NSLocalizedString(@"About BBEP", @"Button title")],
                                  [JSMenuViewItem itemWithIdentifier:JSWebsiteMenuIdentifier title:NSLocalizedString(@"Website", @"Button title")],
                                  [JSMenuViewItem itemWithIdentifier:JSNTIMenuIdentifier title:NSLocalizedString(@"Nan Tien Institute", @"Button title")]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0)
    {
        self.navigationController.navigationBar.tintColor = [[JSConstants sharedConstants] barTintColor];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];

    [self dismissSecondaryMenuViewController:nil completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)applicationDidBecomeActive
{
    if ([[JSDharmaCardDataModel sharedDataModel] shouldShowCardUponLaunch])
    {
        [[JSDharmaCardDataModel sharedDataModel] setShouldShowCardUponLaunch:NO];
        [[self navigationController] popToRootViewControllerAnimated:NO];
        [self showDharmaCardsAnimated:NO];
    }
}

#pragma mark -

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    self.currentMenuItem = self.menuItems[indexPath.row];

    switch (indexPath.row)
    {
        case 0:
            self.currentSecondaryMenuItems = self.buddhaSecondaryMenuItems;
            break;

        case 1:
            self.currentSecondaryMenuItems = self.dharmaSecondaryMenuItems;
            break;

        case 2:

            self.currentSecondaryMenuItems = self.sanghaSecondaryMenuItems;
            break;
    }
    [self performSegueWithIdentifier:JSSecondaryMenuSegueIdentifier sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:JSSecondaryMenuSegueIdentifier])
    {
        JSMenuViewController* menuViewController = (JSMenuViewController*)segue.destinationViewController;
        menuViewController.menuItems = self.currentSecondaryMenuItems;
        menuViewController.delegate = self;
        menuViewController.navigationItem.title = self.currentMenuItem.title;
    }
}

#pragma mark -JSMenuViewControllerDelegate

- (void)menuViewController:(JSMenuViewController*)menuViewController didSelectMenuItem:(JSMenuViewItem*)menuItem
{
    if ([menuItem.identifier isEqualToString:JSTimelineMenuIdentifier])
    {
        [self showTimeline];
    }
    else if ([menuItem.identifier isEqualToString:JSTriptychMenuIdentifier])
    {
        [self showTriptych];
    }
    else if ([menuItem.identifier isEqualToString:JSSilkRoadMenuIdentifier])
    {
        [self showSilkRoad];
    }
    else if ([menuItem.identifier isEqualToString:JSLaterZhaoCarriageMenuIdentifier])
    {
        [self showCarriage];
    }
    else if ([menuItem.identifier isEqualToString:JSDharmaCardsMenuIdentifier])
    {
        [self showDharmaCards];
    }
    else if ([menuItem.identifier isEqualToString:JSQnACardsMenuIdentifier])
    {
        [self showQnACards];
    }
    else if ([menuItem.identifier isEqualToString:JSBodhiTreeMenuIdentifier])
    {
        [self showBodhiTree];
    }
    else if ([menuItem.identifier isEqualToString:JS365DaysOfWisdomMenuIdentifier])
    {
        [self show365DaysWisdom];
    }
    else if ([menuItem.identifier isEqualToString:JSWebsiteMenuIdentifier])
    {
        [self showWebSite];
    }
    else if ([menuItem.identifier isEqualToString:JSAboutBBEPMenuIdentifier])
    {
        [self showAboutBBEP];
    }
    else if ([menuItem.identifier isEqualToString:JSNTIMenuIdentifier])
    {
        [self showNTI];
    }
}

- (void)showTimeline
{
    JSTimelineViewController* timelineViewController = [JSTimelineViewController new];
    [self.navigationController pushViewController:timelineViewController animated:YES];
}

- (void)showTriptych
{
    JSTriptychViewController* triptychViewController = [JSTriptychViewController new];
    [self.navigationController pushViewController:triptychViewController animated:YES];
}

- (void)showSilkRoad
{
    JSSilkRoadViewController* silkRoadViewController = [JSSilkRoadViewController new];
    [self.navigationController pushViewController:silkRoadViewController animated:YES];
}

- (void)showCarriage
{
    JSCarriageViewController* carriageViewController = [JSCarriageViewController new];
    [self.navigationController pushViewController:carriageViewController animated:YES];
}

- (void)showDharmaCards
{
    [self showDharmaCardsAnimated:YES];
}

- (void)showQnACards
{
    JSQnACardsViewController* QnACardsViewController = [JSQnACardsViewController new];
    [self.navigationController pushViewController:QnACardsViewController animated:YES];
}

- (void)showBodhiTree
{
    JSBodhiTreeViewController* bodhiTreeViewController = [JSBodhiTreeViewController new];
    [self.navigationController pushViewController:bodhiTreeViewController animated:YES];
}

- (void)showDharmaCardsAnimated:(BOOL)animated
{
    JSDharmaCardsViewController* dharmaCardsViewController = [JSDharmaCardsViewController new];
    [self.navigationController pushViewController:dharmaCardsViewController animated:animated];
}

- (void)show365DaysWisdom
{
    JS365DaysWisdomViewController* viewController = [JS365DaysWisdomViewController new];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)showWebSite
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:JSWebsiteURL]];
}

- (void)showAboutBBEP
{
    [self performSegueWithIdentifier:@"JSAboutViewController" sender:self];
}

- (void)showNTI
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:JSNTIWebsiteURL]];
}


#pragma mark -

- (IBAction)dashboardButtonTapped:(id)sender
{
    JSDashboardButton* button = sender;
    switch (button.buttonIndex)
    {
        case 0:
            self.currentSecondaryMenuItems = self.buddhaSecondaryMenuItems;
            break;

        case 1:
            self.currentSecondaryMenuItems = self.dharmaSecondaryMenuItems;
            break;

        case 2:
            self.currentSecondaryMenuItems = self.sanghaSecondaryMenuItems;
            break;
    }


    if (self.isSecondaryMenuViewControllerPresented && button == self.buttonSecondaryMenuViewControllerPresentedFrom)
    {
        [self dismissSecondaryMenuViewController:self.iPadSecondaryMenuViewController.tableView completion:^{
                                                                                      }];
    }
    else
    {
        if (self.iPadSecondaryMenuViewController == nil)
        {
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
            self.iPadSecondaryMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"JSSecondaryMenuViewController"];
            [self.iPadSecondaryMenuViewController.view layoutIfNeeded]; // triggers loading it from the xib
            self.iPadSecondaryMenuViewController.delegate = self;
        }

        self.iPadSecondaryMenuViewController.menuItems = self.currentSecondaryMenuItems;
        self.buttonSecondaryMenuViewControllerPresentedFrom = button;

        [self presentSecondaryMenuViewController:self.iPadSecondaryMenuViewController.tableView inRect:CGRectMake(button.frame.origin.x, 0, button.frame.size.width, 400.0)];
    }
}

- (void)presentSecondaryMenuViewController:(UIView*)secondaryView inRect:(CGRect)rect
{
    void (^ presentingBlock)(void) = ^{
        
        secondaryView.translatesAutoresizingMaskIntoConstraints = NO;
        [self embedView:secondaryView intoContainerView:self.secondaryMenuContainerView];
        self.isSecondaryMenuViewControllerPresented = YES;
        self.secondaryMenuLeftConstraint.constant = rect.origin.x;
        [self.view layoutSubviews];
        self.secondaryMenuTopConstraint.constant = -20.0;
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^
        {
            [self.view layoutSubviews];
        }                completion:^(BOOL finished) {
                         }];
    };

    if (self.isSecondaryMenuViewControllerPresented)
    {
        [self dismissSecondaryMenuViewController:self.iPadSecondaryMenuViewController.tableView completion:^{
                                                                                          presentingBlock();
                                                                                      }];
    }
    else
    {
        presentingBlock();
    }
}

- (void)embedView:(UIView*)childView intoContainerView:(UIView*)containerView
{
    if (childView.superview != containerView)
    {
        [containerView addSubview:childView];
        childView.frame = containerView.bounds;

        childView.translatesAutoresizingMaskIntoConstraints = false;

        NSArray* horizontalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[childView]-0-|" options:NSLayoutFormatAlignAllLeft metrics:nil views:@{@"childView":childView}];
        NSArray* verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[childView]-0-|" options:NSLayoutFormatAlignAllTop metrics:nil views:@{@"childView":childView}];
        [containerView addConstraints:horizontalConstraint];
        [containerView addConstraints:verticalConstraints];
    }
}

- (void)dismissSecondaryMenuViewController:(UIView*)secondaryView completion:(void (^)(void))completion
{
    self.secondaryMenuTopConstraint.constant = -self.secondaryMenuContainerView.frame.size.height - 20.0;
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^
    {
        [self.view layoutSubviews];
    }                completion:^(BOOL finished)
    {
        self.isSecondaryMenuViewControllerPresented = NO;
        [secondaryView removeFromSuperview];
        if (completion)
        {
            completion();
        }
    }];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (self.iPadSecondaryMenuViewController)
    {
        [self dismissSecondaryMenuViewController:self.iPadSecondaryMenuViewController.tableView completion:nil];
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    if (self.iPadSecondaryMenuViewController)
    {
        [self dismissSecondaryMenuViewController:self.iPadSecondaryMenuViewController.tableView completion:nil];
    }
}

@end
