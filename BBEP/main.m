//
//  main.m
//  BBEP
//
//  Created by Davyd Geyl on 3/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JSAppDelegate.h"

int main(int argc, char* argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JSAppDelegate class]));
    }
}
