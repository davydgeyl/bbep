//
//  JSMainViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 11/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSMainViewController.h"
#import "JSTimelineViewController.h"
#import "JSTriptychViewController.h"
#import "JSSilkRoadViewController.h"
#import "JSDharmaCardsViewController.h"
#import "JSConstants.h"
#import "JSActionCellBackgroundView.h"
#import "JSCarriageViewController.h"
#import "JSWebViewController.h"
#import "JSShadowFold.h"
#import "JSDharmaCardDataModel.h"
#import "JS365DaysWisdomViewController.h"

NSString *const JSActionCellIdentifier = @"JSActionCellIdentifier";
CGFloat const JSActionCellRowHeight = 32.0;
NSUInteger const JSActionCellsCount = 7;
NSUInteger const JSVisibleActionCellsCount = 6;
CGFloat const JSActionTableViewLandscapeWidth = 250.0;
CGFloat const JSActionsTableViewHeight = JSActionCellRowHeight * JSVisibleActionCellsCount;
CGFloat const JSActionsTableViewLandscapeHeight = JSActionCellRowHeight * JSActionCellsCount;

CGFloat const JSActionsTableViewHorizontalMargin = 8.0;
CGFloat const JSActionsTableViewBottomMargin = 8.0;

@interface JSMainViewController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) JSTimelineViewController *timelineViewController;

@end

@implementation JSMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];

	if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0) {
		self.navigationController.navigationBar.tintColor = [[JSConstants sharedConstants] barTintColor];
	}

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];

	_bottomFold.foldType = JSShadowFoldBottom;
	[self updateFolds];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
	[self updateViews];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
	[self updateViews];
}

- (void)updateViews {
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
		CGRect bounds = self.view.bounds;
		UIInterfaceOrientation interfaceOrientation = self.interfaceOrientation;

		CGFloat titleBarHeight = [[JSConstants sharedConstants] titleBarHeightForInterfaceOrientation:interfaceOrientation];

		if ((interfaceOrientation == UIInterfaceOrientationPortrait) || (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)) {
			_actionsTableView.frame = CGRectMake(JSActionsTableViewHorizontalMargin, bounds.size.height - JSActionsTableViewHeight - JSActionsTableViewBottomMargin, bounds.size.width - JSActionsTableViewHorizontalMargin * 2.0, JSActionsTableViewHeight);

			_descriptionView.frame = CGRectMake(0, titleBarHeight, bounds.size.width, bounds.size.height - _actionsTableView.frame.size.height - titleBarHeight - JSActionsTableViewBottomMargin);
		}
		else {
			CGFloat actionsTableViewWidth = JSActionTableViewLandscapeWidth;

			_actionsTableView.frame = CGRectMake(bounds.size.width - actionsTableViewWidth - JSActionsTableViewHorizontalMargin, titleBarHeight + (bounds.size.height - titleBarHeight - JSActionsTableViewLandscapeHeight) / 2.0, actionsTableViewWidth, JSActionsTableViewLandscapeHeight);

			_descriptionView.frame = CGRectMake(0, titleBarHeight, bounds.size.width - actionsTableViewWidth - JSActionsTableViewHorizontalMargin, bounds.size.height - titleBarHeight);
		}

		[_descriptionTextView scrollRangeToVisible:NSMakeRange(0, 1)];
		[self updateFolds];
	}
}

- (void)navigateBack {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)showTimeline:(id)sender {
	if (_timelineViewController == nil)
		_timelineViewController = [JSTimelineViewController new];
	[self.navigationController pushViewController:_timelineViewController animated:YES];
}

- (IBAction)showTriptych:(id)sender {
	JSTriptychViewController *triptychViewController = [JSTriptychViewController new];
	[self.navigationController pushViewController:triptychViewController animated:YES];
}

- (IBAction)showSilkRoad:(id)sender {
	JSSilkRoadViewController *silkRoadViewController = [JSSilkRoadViewController new];
	[self.navigationController pushViewController:silkRoadViewController animated:YES];
}

- (void)showCarriage {
	JSCarriageViewController *carriageViewController = [JSCarriageViewController new];
	[self.navigationController pushViewController:carriageViewController animated:YES];
}

- (void)showDharmaCards {
	[self showDharmaCardsAnimated:YES];
}

- (void)showDharmaCardsAnimated:(BOOL)animated {
	JSDharmaCardsViewController *dharmaCardsViewController = [JSDharmaCardsViewController new];
	[self.navigationController pushViewController:dharmaCardsViewController animated:animated];
}

- (void)show365DaysWisdom {
	JS365DaysWisdomViewController *viewController = [JS365DaysWisdomViewController new];
	[self.navigationController pushViewController:viewController animated:YES];
}

- (void)showWebSite {
	JSWebViewController *webViewController = [JSWebViewController new];
	webViewController.linkURL = [NSURL URLWithString:JSWebsiteURL];
	[self.navigationController pushViewController:webViewController animated:YES];
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
	return JSActionCellsCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:JSActionCellIdentifier];

	if (cell == nil) {
		cell = [UITableViewCell new];
		cell.textLabel.textAlignment = NSTextAlignmentCenter;
		cell.textLabel.textColor = [UIColor colorWithHue:210.0 / 360.0 saturation:1.0 brightness:0.5 alpha:1.0];
		cell.textLabel.font = [UIFont fontWithName:@"Lucida Grande" size:14.0];
		cell.backgroundView = [JSActionCellBackgroundView new];
		JSActionCellBackgroundView *selectedBackgroundView = [JSActionCellBackgroundView new];
		selectedBackgroundView.selected = YES;
		cell.selectedBackgroundView = selectedBackgroundView;
	}

	NSUInteger row = indexPath.row;
	if (row == 0) {
		cell.textLabel.text = NSLocalizedString(@"Dharma Cards", @"Button title");
	}
	else if (row == 1) {
		cell.textLabel.text = NSLocalizedString(@"366 Days with Wisdom", @"Button title");
	}
	else if (row == 2) {
		cell.textLabel.text = NSLocalizedString(@"Timeline", @"Button title");
	}
	else if (row == 3) {
		cell.textLabel.text = NSLocalizedString(@"Triptych", @"Button title");
	}
	else if (row == 4) {
		cell.textLabel.text = NSLocalizedString(@"Silk Road", @"Button title");
	}
	else if (row == 5) {
		cell.textLabel.text = NSLocalizedString(@"Later Zhao Carriage Design", @"Button title");
	}
	else if (row == 6) {
		cell.textLabel.text = NSLocalizedString(@"Website", @"Button title");
	}

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	NSUInteger row = indexPath.row;
	if (row == 0) {
		[self showDharmaCards];
	}
	else if (row == 1) {
		[self show365DaysWisdom];
	}
	else if (row == 2) {
		[self showTimeline:nil];
	}
	else if (row == 3) {
		[self showTriptych:nil];
	}
	else if (row == 4) {
		[self showSilkRoad:nil];
	}
	else if (row == 5) {
		[self showCarriage];
	}
	else if (row == 6) {
		[self showWebSite];
	}
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	if (scrollView == _descriptionTextView) {
		[self updateFolds];
	}
}

- (void)updateFolds {
	CGFloat verticalOffset = _descriptionTextView.contentOffset.y;
	CGFloat relativeOffset = verticalOffset / _topFold.frame.size.height;

	CGFloat topExpansion = MIN(relativeOffset, 1.0);
	_topFold.expansion = MAX(topExpansion, 0.0);

	CGFloat bottomOffset = _descriptionTextView.contentSize.height - _descriptionTextView.bounds.size.height - _descriptionTextView.contentOffset.y;
	CGFloat relativeBottomOffset = bottomOffset / _bottomFold.frame.size.height;

	CGFloat bottomExpansion = MIN(relativeBottomOffset, 1.0);
	_bottomFold.expansion = MAX(bottomExpansion, 0.0);
}

- (void)applicationDidBecomeActive {
	if ([[JSDharmaCardDataModel sharedDataModel] shouldShowCardUponLaunch]) {
		[[JSDharmaCardDataModel sharedDataModel] setShouldShowCardUponLaunch:NO];
		[[self navigationController] popToRootViewControllerAnimated:NO];
		[self showDharmaCardsAnimated:NO];
	}
}

- (IBAction)showBuddhaMenu:(id)sender {
}

- (IBAction)showDharmaMenu:(id)sender {
}

- (IBAction)showSanghaMenu:(id)sender {
}

@end
