//
//  JSCardView.h
//  BBEP
//
//  Created by Davyd Geyl on 6/03/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JSCardView;

@protocol JSCardViewDelegate <NSObject>

- (void)cardViewDidTapInside:(JSCardView *)cardView;

@end

@interface JSCardView : UIView

@property (nonatomic, weak) id <JSCardViewDelegate> delegate;
@property (nonatomic, strong) NSString *imageName;

@end
