    //
//  JSGradientLayer.m
//  BBEP
//
//  Created by Davyd Geyl on 6/03/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSGradientLayer.h"
#import "JSConstants.h"

@interface JSGradientLayer ()

@property (nonatomic, assign) CGGradientRef gradient;

@end

@implementation JSGradientLayer

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.color = [UIColor greenColor];
    }
    return self;
}


- (void)setColor:(UIColor *)color
{
    _color = color;
    CGFloat locations[] = {0, 1};
    
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat alpha;
    
    [color getRed:&red green:&green blue:&blue alpha:&alpha];
    
    CFMutableArrayRef colors = CFArrayCreateMutable(nil, 2, nil);
    
    CFArrayAppendValue(colors, [UIColor clearColor].CGColor);
    CFArrayAppendValue(colors, color.CGColor);
    
    _gradient = CGGradientCreateWithColors([JSConstants sharedConstants].colorSpaceDeviceRGB, colors, locations);
    
    CFRelease(colors);
    
    [self setNeedsDisplay];
}

- (void)drawInContext:(CGContextRef)ctx
{
    CGContextDrawLinearGradient(ctx, _gradient, CGPointMake(0, 0), CGPointMake(0, self.bounds.size.height), 0);
}

@end
