//
//  JSQnACard.h
//  BBEP
//
//  Created by Davyd Geyl on 5/03/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSQnACard : NSObject

@property (nonatomic, strong) NSNumber* score;
@property (nonatomic, strong) NSString* question;

@property (nonatomic, strong) NSString* answerRawString;
@property (nonatomic, strong) NSString* answer;
@property (nonatomic, copy) NSArray* answerHighlightedRanges;

@property (nonatomic, strong) NSString* backgroundImageName;

@property (nonatomic, readonly) NSAttributedString* attributedAnswer;

@end
