//
//  JSGradientLayer.h
//  BBEP
//
//  Created by Davyd Geyl on 6/03/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface JSGradientLayer : CALayer

@property (nonatomic, strong) UIColor* color;

@end
