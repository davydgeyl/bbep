//
//  JSQnACard.m
//  BBEP
//
//  Created by Davyd Geyl on 5/03/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSQnACard.h"

static NSString* const JSAnswerHighlightStartTag = @"<";
static NSString* const JSAnswerHighlightEndTag = @">";


@interface JSQnACard ()

@property (nonatomic, readwrite) NSAttributedString* attributedAnswer;

@end

@implementation JSQnACard

- (void)setAnswerRawString:(NSString*)answerRawString
{
    _answerRawString = answerRawString;
    [self updateAnswerStringAndHighlightedRanges];
}

//- (void)updateAttributedAnswerIfNeeded {
//	if (self.answer && self.answerRanges) {
//		[self updateAttributedAnswer];
//	}
//}

- (void)updateAttributedAnswer
{
//	NSMutableAttributedString *attributedAnswer = [[NSMutableAttributedString alloc] initWithString:self.answer attributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Monotype Corsiva" size:14.0] }];
//
//	for (NSValue *range in self.answerRanges) {
//		[attributedAnswer addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range.rangeValue];
//	}
//
//	self.attributedAnswer = attributedAnswer;
}

- (void)updateAnswerStringAndHighlightedRanges
{
    NSMutableArray* answerHighlightedRanges = [NSMutableArray new];
    NSMutableString* answerString = [NSMutableString stringWithString:self.answerRawString];
    NSUInteger highlightLocation = [answerString rangeOfString:JSAnswerHighlightStartTag].location;

    while (highlightLocation < NSNotFound)
    {
        NSUInteger highlightLength = [answerString rangeOfString:JSAnswerHighlightEndTag].location - highlightLocation - 1;

        if (highlightLength == 0)
        {
            NSLog(@"Cannot find closing highlight tag in answer: %@", self.answerRawString);
            return;
        }

        NSRange highlightedRange = NSMakeRange(highlightLocation, highlightLength);
        NSRange fullRange = NSMakeRange(highlightLocation, highlightLength + 1);
        NSValue* highlightedRangeValue = [NSValue valueWithRange:highlightedRange];
        [answerHighlightedRanges addObject:highlightedRangeValue];

        [answerString replaceOccurrencesOfString:JSAnswerHighlightStartTag withString:@"" options:0 range:fullRange];
        [answerString replaceOccurrencesOfString:JSAnswerHighlightEndTag withString:@"" options:0 range:fullRange];

        highlightLocation = [answerString rangeOfString:JSAnswerHighlightStartTag].location;
    }

    self.answerHighlightedRanges = answerHighlightedRanges;
    self.answer = answerString;
}

@end
