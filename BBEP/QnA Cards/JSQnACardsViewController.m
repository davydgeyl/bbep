//
//  JSQnACardsViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 4/03/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSQnACardsViewController.h"
#import "JSQnACardsDataModel.h"
#import "JSCardView.h"
#import "JSQnACard.h"
#import "JSQuestionViewController.h"
#import "JSAnswerViewController.h"

@interface JSQnACardsViewController () <JSCardViewDelegate>

@property (nonatomic, strong) IBOutlet UIView* cardView;
@property (nonatomic, strong) IBOutlet UIButton* nextCardButton;
@property (nonatomic, strong) IBOutlet UIButton* toggleAnswerQuestionButton;
@property (nonatomic, strong) IBOutlet UILabel* hintLabel;
@property (nonatomic, strong) JSQnACardsDataModel* dataModel;
@property (nonatomic, assign) BOOL isShowingAnswer;

@property (nonatomic, strong) JSQuestionViewController* questionViewController;
@property (nonatomic, strong) JSAnswerViewController* answerViewController;

@property (nonatomic, strong) UIButton* titleButton;

@end

@implementation JSQnACardsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.dataModel = [JSQnACardsDataModel sharedModel];

    self.cardView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.cardView.layer.shadowRadius = 1.0;
    self.cardView.layer.shadowOpacity = 0.65;
    self.cardView.layer.shadowOffset = CGSizeMake(0, 1.0);
    self.cardView.layer.cornerRadius = 16.0;
    self.cardView.layer.allowsEdgeAntialiasing = YES;

    self.questionViewController = [JSQuestionViewController new];
    self.questionViewController.view.layer.cornerRadius = 16.0;

    self.answerViewController = [JSAnswerViewController new];
    self.answerViewController.view.layer.cornerRadius = 16.0;

    [self embedView:self.questionViewController.view intoContainerView:self.cardView];

    [self update];

    UIBarButtonItem* plusButton = [[UIBarButtonItem alloc] initWithTitle:@"   +   " style:UIBarButtonItemStylePlain target:self action:@selector(addPoints:)];
    self.navigationItem.rightBarButtonItem = plusButton;

    CGFloat margin = 48.0;
    _titleButton = [[UIButton alloc] initWithFrame:CGRectMake(margin, 0, self.view.bounds.size.width - margin * 2.0, 48.0)];
    [_titleButton setTitle:@"button" forState:UIControlStateNormal];
    [_titleButton addTarget:self action:@selector(titleTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_titleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.navigationItem.titleView = _titleButton;

    [self updatePoints];
}

- (void)update
{
    JSQnACard* card = self.dataModel.cards[self.dataModel.currentCardNumber];
    self.questionViewController.card = card;
    self.answerViewController.card = card;
    
    self.hintLabel.hidden = self.dataModel.currentCardNumber > 0;
    self.nextCardButton.hidden = self.dataModel.currentCardNumber == 0;
}

- (IBAction)toggleAnswerQuestion:(id)sender
{
    if (self.isShowingAnswer)
    {
        [self showQuestion];
        self.isShowingAnswer = NO;
    }
    else
    {
        [self showAnswer];
        self.isShowingAnswer = YES;
    }
}

- (IBAction)showNextQuestion:(id)sender
{
    [self.dataModel changeToNextCard];
    [self update];
    [self showQuestion];
    //self.nextCardButton.hidden = YES;
    self.nextCardButton.hidden = NO;
}

- (void)showAnswer
{
    [self embedView:self.answerViewController.view intoContainerView:self.cardView];

    [UIView transitionFromView:self.questionViewController.view
                        toView:self.answerViewController.view
                      duration:1.0
                       options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionTransitionFlipFromRight
                    completion:^(BOOL finished)
    {
        [self.questionViewController.view removeFromSuperview];
        self.nextCardButton.hidden = NO;
    }];

    self.nextCardButton.hidden = NO;
    self.hintLabel.hidden = YES;
}

- (void)showQuestion
{
    [self embedView:self.questionViewController.view intoContainerView:self.cardView];

    [UIView transitionFromView:self.answerViewController.view
                        toView:self.questionViewController.view
                      duration:1.0
                       options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionTransitionFlipFromLeft
                    completion:^(BOOL finished)
    {
        [self.answerViewController.view removeFromSuperview];
    }];
    self.isShowingAnswer = NO;
}

#pragma mark - Card View Delegate

- (void)cardViewDidTapInside:(JSCardView*)cardView
{
    [self toggleAnswerQuestion:nil];
}

- (void)embedView:(UIView*)childView intoContainerView:(UIView*)containerView
{
    if (childView.superview != containerView)
    {
        childView.translatesAutoresizingMaskIntoConstraints = false;
        childView.frame = containerView.bounds;
        [containerView addSubview:childView];

        NSArray* horizontalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[childView]-0-|" options:NSLayoutFormatAlignAllLeft metrics:nil views:@{@"childView":childView}];
        NSArray* verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[childView]-0-|" options:NSLayoutFormatAlignAllTop metrics:nil views:@{@"childView":childView}];
        [containerView addConstraints:horizontalConstraint];
        [containerView addConstraints:verticalConstraints];
    }
}

#pragma mark -

- (void)updatePoints
{
    NSString* myPoints;

    if (self.dataModel.myPoints == 1)
    {
        myPoints = [NSString stringWithFormat:@"%d point", (int)self.dataModel.myPoints];
    }
    else
    {
        myPoints = [NSString stringWithFormat:@"%d points", (int)self.dataModel.myPoints];
    }

    [self.titleButton setTitle:(self.dataModel.myPoints > 0 ? myPoints : NSLocalizedString(@"no points", @"Title text")) forState:UIControlStateNormal];
    self.navigationItem.prompt = self.dataModel.myPoints > 0 ? nil : NSLocalizedString(@"Tap plus to add points for correct answers", @"Navigation prompt");
}

- (void)addPoints:(id)sender
{
    [self.dataModel addToMyPoints];
    [self updatePoints];
}

- (void)titleTapped:(UIGestureRecognizer*)sender
{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Reset points", @"Alert title") message:@"Do you want to reset the points and start collecting them again?" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Button title") otherButtonTitles:NSLocalizedString(@"Reset", @"Button title"), nil];
    [alertView show];
}

- (void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self.dataModel resetMyPoints];
        [self updatePoints];
    }
}

@end
