English (answer within 150 characters)
----
Q: What are the "Three Acts of Goodness"?
A: The "Three Acts of Goodness" are to do good deeds, to speak good words and to think good thoughts.
----
Q: How does one "speak good words?"
A: One speaks good words by ensuring that every word spoken is compassionate, kind and encouraging.
----
Q: How does one "do good deeds?"
A: One does good deeds by ensuring that every deed is kind, helpful and beneficial to all.
----
Q: How does one "think good thoughts?"
A: One thinks good thoughts by ensuring that every intention benefits others. 
----
Q: Where can one find happiness?
A: Happiness has always dwelt within our own hearts and minds.  There is no use in praying to the divine or begging the Buddha to find happiness.
----
Q: How does one survive in a world of imperfections? 
A: If we strive for the better half, the worse half will naturally lessen.  To live holistically, accept the beautiful half and tolerate the flawed half. 
----
Q: How does one create a beautiful world? 
A: A beautiful world starts with oneself.  If, for example, you feel neglected in your relationships, you should exude warmth, starting with yourself.
----
Q: What is the Buddhist perspective on happiness and prosperity? 
A: Making the most of present causes and conditions, while creating causes and conditions for future prosperity  ensures wealth and good fortune will follow.
----
Q: How can one be successful at work and in life?
A: To be successful, one must endure the unendurable and do the seemingly impossible.
----
Q: How does one find peace of mind?
A: Make the best of all situations.  Instead of complaining about the muddy ground after the rain, appreciate the sunlight illuminating the path.
----
Q: What can one do to protect the right of animals to live?
A: One should respect all life, by allowing sentient beings to live without fear and providing them with a safe environment.  
----
Q: How does one acquire great merits?
A: We receive great merits through tolerating and bearing what others will not .  Be patient to all beings.
----
Q: What does "wealth" mean to a Buddhist?
A: To a Buddhist, "wealth" means being a grateful person, who knows how to love and how to give. A life filled with gratitude is a "wealthy" life.
----
Q: What does "letting go" mean?
A: "Letting go" is the ability to pick up and let go with ease.  Just like picking up a suitcase when it's time to travel, and putting it down when you return.  
----
Q: How can one be truly happy? 
A: Happiness is sharing what we have with others and not being envious of what they possess.  This is the truest form of happiness.
----
Q: What is one's most valuable possession? 
A: The most valuable possession is happiness.  Money, fame, and fortune are unimportant.  Having it all is meaningless if we are unhappy.
----
Q: How does one succeed in life?
A: Strength and merit are accumulated by enduring ridicule, blame and criticism while remaining calm, without self justification or argument.  
----
Q: How does one live harmoniously with others? 
A: Allow others to be right, and be willing to be wrong. Allow others to be happy, and be willing to suffer.  Learn from the compassionate bodhisattvas.
----
Q: What is the key to successful cultivation?
A: The key is to transform negative circumstances into positive experiences; and to have the courage to accept and transform defilements into Bodhi. 
----
Q: How can one "live in the moment"?
A: If it is of benefit and contributes to society, seize the present moment  to work diligently. The wise live neither in the past nor the future.
----
Q: What is the mind like?
A: The mind can be a sovereign issuing orders, an ever flowing stream, or an artist painting a picture.  It is vast and boundless.  
----
Q: What is universal love or compassion? 
A: The expansive love from the love of self and family, to a love for everyone  in society, our country, and throughout the world, by means of wisdom and respect.
----
Q: How can one become a Buddha?
A: By always keeping in mind that "I am a Buddha."  When our every thought is the Buddha, it is as if we are blessed and have the strength of a Buddha.
----
Q: How does one practise "Do not kill?"
A: No killing means respecting the lives of others, as well as protecting all life.
----
Q: How does one practise "Do not steal?"
A: No stealing means respecting the property of others, as well as being generous and helpful.
----
Q: How does one practise "No sexual misconduct?"
A: No sexual misconduct means respecting the body of others, and also protecting their integrity.
----
Q: How does one practise "Do not lie?"
A: No lying means respecting our own reputation, as well as publicising others' virtues .
----
Q: How does one practise "No taking of intoxicants?"
A: No intoxicants means respecting our own intellect, as well as learning to be wise by helping others maintain clear thinking.
----
Q: How does one respect the rights of others?
A: One respects the rights of others by upholding the five precepts, and by not giving in to aggression.
----
Q: How does one respect the value of life?
A: One respects the value of life by offering compassion instead of killing.
----
Q: How does one respect the environment?
A: One respects nature and protects the environment by practicing conservation instead of destruction.
----
Q: How does a Chan practitioner achieve enlightenment? 
A: A Chan practitioner's daily labours help support him on the path.  Many Chan masters became enlightened while shouldering heavy workloads.
----
Q: How should one see the world?
A: The world is much more accurately perceived if we use our minds' eye, instead of our physical eyes.
----
Q: How does one enter heaven?
A: When our minds are pure, open and on the right path at every moment, we are already living in heaven.
----
Q: What is the best gift in the world? 
A: Give others kind words, a smile, good intention and a helping hand. Give others confidence, hope, joy and convenience.
----
Q: What is love? 
A: Love is being of benefit to others, helping them succeed, offering respect and allowing them freedom.  It is virtuous, truthful, and pure.
----
Q: How should one strive for world peace?
A: Through the Five Harmonies. Harmony within oneself, the family, interpersonal relationships and society, ensures that world peace will follow.
----
Q:  How should one deal with changing circumstances?
A: Though causes and conditions are ever changing, one must remain unshakeable when facing gain, loss, slander, repute, praise, ridicule, suffering and happiness.
----
Q: How does one practise "giving?"
A: By giving without bounds , irrespective of time or place.  When we give joy, confidence, hope and convenience, we add  happiness and peace to our world.
----
Q: How should we plan our life?
A: The best plan for life is to live with meaning and worth. Plan for a life of awakening  and self-liberation.  Benefit others by living ethically and wisely.  
----
Q: What is the difference between a rich person and a poor person?
A: Those who are always willing to help others are rich at heart, while those who are greedy and never content are poor.
----
Q: What makes a good Buddhist?
A: A good Buddhist is mindful  of the law of cause and effect , forgets grudges and gossip, studies the teachings, and thrives on loving kindness, compassion, joy and equanimity.
----
Q: How does our behaviour determine our destiny?
A: Repeated actions become habit. Habit becomes character.  Character becomes fate.  Fate rules our lives.
----
Q: How does a person become successful in life?
A: A successful person can gain strength from adversity  and accept criticism without anger but with loving kindness,compassion, joy and equanimity.
----
Q: Why do we have mishaps?
A: Mishaps befall us as part of our practice . Just as trees grow through heat and rain, our minds can only be purified - with strict training.
----
Q: What does it mean to be "busy"?
A: To be truly "busy" is always keeping our mind focused  and our hands doing productive work. Keeping busy is adding vitality to life. 
----
Q: Why can't one's life be always happy and successful?
A: Life with happiness and suffering is enriched; with success and failure is reasonable; with gain and loss is fair and with birth and death is natural.
----
Q: What should one pursue in life?
A:  Pursue a genuine , virtuous and beautiful life through sincerity, kindness and making the world a better place. 
----
Q: What does it mean to be rich?
A: To be rich means to be happy. Happiness, not money, is real wealth; while ignorance is real poverty.
----
Q: How does one overcome life's hurdles?
A: If we do not give up in the face of setbacks, we can cultivate our strengths. By working diligently and being willing to learn, we can break free of limitations.
----
Q: What is the formula for success?
A: To know that success comes from hardship , celebrity is forged by pressure, honour comes from effort and achievement requires sacrifice and dedication. 
----
Q: How does one cultivate in daily life?
A: One cultivates by giving without greed, by keeping busy without bitterness, by working diligently and by being patient. 




