//
//  JSAnswerViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 17/06/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSAnswerViewController.h"
#import "JSGradientView.h"
#import "JSQnACard.h"
#import "JSQnACard+ViewModel.h"

@interface JSAnswerViewController ()

@property (nonatomic, strong) IBOutlet UIImageView* imageView;
@property (nonatomic, strong) IBOutlet UILabel* answerLabel;
@property (nonatomic, strong) IBOutlet UIView* textContainerView;
@property (nonatomic, strong) IBOutlet JSGradientView* headerView;
@property (nonatomic, strong) IBOutlet UIView* pointsBackgroundView;
@property (nonatomic, strong) IBOutlet UILabel* pointsNumberLabel;
@property (nonatomic, strong) IBOutlet UILabel* pointsTextLabel;
@property (nonatomic, strong) NSDictionary* generalAttributes;
@property (nonatomic, strong) NSDictionary* highlightedAttributes;

@end

@implementation JSAnswerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.textContainerView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.95];
    self.headerView.color = self.textContainerView.backgroundColor;

    NSRangePointer effectiveRange = nil;
    self.generalAttributes = [self.answerLabel.attributedText attributesAtIndex:0 effectiveRange:effectiveRange];

    self.highlightedAttributes = [self.answerLabel.attributedText attributesAtIndex:self.answerLabel.attributedText.length - 1 effectiveRange:effectiveRange];

    self.pointsBackgroundView.layer.cornerRadius = CGRectGetWidth(self.pointsBackgroundView.frame) / 2.0;
}

- (void)setCard:(JSQnACard*)card
{
    _card = card;
    [self update];
}

- (void)update
{
    self.imageView.image = [UIImage imageNamed:self.card.backgroundImageName];
    NSMutableAttributedString* attributedAnswer = [[NSMutableAttributedString alloc] initWithString:self.card.answer attributes:self.generalAttributes];

    for (NSValue* rangeValue in self.card.answerHighlightedRanges)
    {
        NSRange range = rangeValue.rangeValue;
        [attributedAnswer setAttributes:self.highlightedAttributes range:range];
    }
    self.answerLabel.attributedText = attributedAnswer;

    NSUInteger score = self.card.score.integerValue;
    self.pointsNumberLabel.text = [NSString stringWithFormat:@"%d", (int)score];
    self.pointsTextLabel.text = score > 1 ? NSLocalizedString(@"points", @"more then 1 point") : NSLocalizedString(@"point", @"1 point");
    self.pointsBackgroundView.backgroundColor = self.card.backgroundColor;
}

@end
