//
//  JSCardTextView.h
//  BBEP
//
//  Created by Davyd Geyl on 9/03/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSGradientView : UIView

@property (nonatomic, strong) UIColor* color;

@end
