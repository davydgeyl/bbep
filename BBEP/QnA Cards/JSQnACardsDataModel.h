//
//  JSQnACardsDataModel.h
//  BBEP
//
//  Created by Davyd Geyl on 4/03/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSQnACardsDataModel : NSObject

@property (nonatomic, strong) NSArray* cards;
@property (nonatomic, assign) NSUInteger currentCardNumber;

+ (JSQnACardsDataModel*)sharedModel;
- (void)changeToNextCard;

@property (nonatomic, readonly) NSUInteger myPoints;
- (void)addToMyPoints;
- (void)resetMyPoints;

@end
