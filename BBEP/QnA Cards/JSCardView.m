//
//  JSCardView.m
//  BBEP
//
//  Created by Davyd Geyl on 6/03/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSCardView.h"
#import "JSCardLayer.h"
#import "JSGradientView.h"

@interface JSCardView ()

@property (nonatomic, strong) JSCardLayer* currentCardLayer;
@property (nonatomic, strong) IBOutlet JSGradientView* cardTextView;
@property (nonatomic, strong) UITapGestureRecognizer* tapGestureRecognizer;

@end

@implementation JSCardView

+ (Class)layerClass
{
    return [JSCardLayer class];
}

- (instancetype)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    _tapGestureRecognizer = [UITapGestureRecognizer new];
    [self addGestureRecognizer:_tapGestureRecognizer];
    [_tapGestureRecognizer addTarget:self action:@selector(tapInside:)];
}

- (void)tapInside:(UIGestureRecognizer*)gestureRecognizer
{
    [self.delegate cardViewDidTapInside:self];
}

- (void)setImageName:(NSString*)imageName
{
    _imageName = imageName;
    [self displayCardWithImage];
}

- (void)displayCardWithImage
{
    self.currentCardLayer = [[JSCardLayer alloc] initWithImageName:self.imageName layerScale:1.0];
}

- (void)setCurrentCardLayer:(JSCardLayer*)currentCardLayer
{
    JSCardLayer* previousLayer = _currentCardLayer;

    currentCardLayer.hidden = YES;
    _currentCardLayer = currentCardLayer;
    [self layoutCurrentLayer];
    [self.layer addSublayer:_currentCardLayer];

    [UIView animateWithDuration:2.0 animations:^
    {
        previousLayer.hidden = YES;
    }

                     completion:^(BOOL finished)
    {
        [previousLayer removeFromSuperlayer];
        [CATransaction begin];
        [CATransaction setAnimationDuration:4.0];
        _currentCardLayer.hidden = NO;
        [CATransaction commit];
    }];
}

- (void)layoutSubviews
{
    [self layoutCurrentLayer];
}

- (void)layoutCurrentLayer
{
    _currentCardLayer.position = CGPointMake(round(self.bounds.size.width / 2.0), round(self.bounds.size.height / 2.0));
}

@end
