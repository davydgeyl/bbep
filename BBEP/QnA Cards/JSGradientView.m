//
//  JSCardTextView.m
//  BBEP
//
//  Created by Davyd Geyl on 9/03/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSGradientView.h"
#import "JSGradientLayer.h"

@implementation JSGradientView

+ (Class)layerClass
{
    return [JSGradientLayer class];
}

- (instancetype)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.backgroundColor = [UIColor clearColor];
    self.opaque = NO;
}

- (void)setColor:(UIColor*)color
{
    [(JSGradientLayer*)self.layer setColor:color];
}

- (UIColor*)color
{
    return [(JSGradientLayer*)self.layer color];
}

@end
