//
//  JSQnACardsDataModel.m
//  BBEP
//
//  Created by Davyd Geyl on 4/03/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSQnACardsDataModel.h"
#import "JSQnACard.h"

static NSUInteger const JSQnACardsNumber = 52;

static NSString *const JSQnACardsDataModelMyPointsKey = @"JSQnACardsDataModelMyPointsKey";


@interface JSQnACardsDataModel ()

@property (nonatomic, assign) NSUInteger myPoints;

@end

@implementation JSQnACardsDataModel

+ (JSQnACardsDataModel*)sharedModel
{
    static JSQnACardsDataModel* sharedModel = nil;
    if (sharedModel == nil)
        sharedModel = [JSQnACardsDataModel new];

    return sharedModel;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSArray* groups = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"QnA" ofType:@"plist"]];

        NSMutableArray* cards = [NSMutableArray new];

        for (NSDictionary* groupDictionary in groups)
        {
            NSNumber* score = groupDictionary[@"score"];

            NSArray* cardDictionaries = groupDictionary[@"cards"];
            for (NSDictionary* cardDictionary in cardDictionaries)
            {
                JSQnACard* card = [JSQnACard new];
                card.score = score;
                card.question = cardDictionary[@"q"];
                card.answerRawString = cardDictionary[@"a"];
                card.backgroundImageName = cardDictionary[@"backgroundImage"];
                [cards addObject:card];
            }
        }

        self.cards = cards;
        
        _myPoints = [[NSUserDefaults standardUserDefaults] integerForKey:JSQnACardsDataModelMyPointsKey];
    }
    return self;
}

- (void)changeToNextCard
{
    NSUInteger nextCardNumber = arc4random_uniform(JSQnACardsNumber);
    
    if (nextCardNumber >= JSQnACardsNumber)
        nextCardNumber = 0;

    self.currentCardNumber = nextCardNumber;
}

- (void)addToMyPoints {
    JSQnACard* currentCard = self.cards[self.currentCardNumber];
    [self setMyPoints:_myPoints + currentCard.score.integerValue];
}

- (void)setMyPoints:(NSUInteger)myPoints {
    _myPoints = myPoints;
    
    [[NSUserDefaults standardUserDefaults] setInteger:_myPoints forKey:JSQnACardsDataModelMyPointsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)resetMyPoints
{
    [self setMyPoints:0];
}

@end
