//
//  JSQuestionViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 16/06/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSQuestionViewController.h"
#import "JSGradientView.h"
#import "JSQnACard.h"
#import "JSQnACard+ViewModel.h"

@interface JSQuestionViewController ()

@property (nonatomic, strong) IBOutlet UIImageView* imageView;
@property (nonatomic, strong) IBOutlet UILabel* questionLabel;
@property (nonatomic, strong) IBOutlet UIView* textContainerView;
@property (nonatomic, strong) IBOutlet JSGradientView* headerView;

@end

@implementation JSQuestionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)setCard:(JSQnACard*)card
{
    _card = card;
    [self update];
}

- (void)update
{
    self.imageView.image = [UIImage imageNamed:self.card.backgroundImageName];
    self.questionLabel.text = self.card.question;
    self.textContainerView.backgroundColor = self.card.backgroundColor;
    self.headerView.color = self.textContainerView.backgroundColor;
}

@end
