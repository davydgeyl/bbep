//
//  JSQuestionViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 16/06/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JSQnACard;

@interface JSQuestionViewController : UIViewController

@property (nonatomic, strong) JSQnACard* card;

@end
