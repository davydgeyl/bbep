//
//  JSQnACard+ViewModel.m
//  BBEP
//
//  Created by Davyd Geyl on 29/06/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSQnACard+ViewModel.h"

@implementation JSQnACard (JSQnACard)

- (UIColor*)backgroundColor
{
    UIColor* result = nil;
    switch (self.score.integerValue)
    {
        case 1:
            result = [UIColor colorWithHue:110.0 / 360.0 saturation:0.57 brightness:0.44 alpha:1.0];
            break;
            
        case 2:
            result = [UIColor colorWithHue:353.0 / 360.0 saturation:0.92 brightness:0.72 alpha:1.0];
            break;
            
        case 3:
        case 4:
            result = [UIColor colorWithHue:299.0 / 360.0 saturation:0.34 brightness:0.51 alpha:1.0];
            break;
            
        case 5:
        case 6:
            result = [UIColor colorWithHue:40.0 / 360.0 saturation:0.87 brightness:0.94 alpha:1.0];
            break;
            
        default:
            break;
    }
    return result;
}

@end
