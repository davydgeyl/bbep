//
//  JSQnACard+ViewModel.h
//  BBEP
//
//  Created by Davyd Geyl on 29/06/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSQnACard.h"

@interface JSQnACard (JSQnACard)

- (UIColor*)backgroundColor;

@end
