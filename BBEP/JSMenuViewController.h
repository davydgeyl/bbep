//
//  JSSecondLevelMenuViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 24/02/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JSMenuViewController;

@interface JSMenuViewItem : NSObject
+ (instancetype)itemWithIdentifier:(NSString*)itemID title:(NSString*)title;
+ (instancetype)itemWithIdentifier:(NSString*)itemID title:(NSString*)title imageName:(NSString*)imageName;
@property (nonatomic, strong) NSString* identifier;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* imageName;
@end

@protocol JSMenuViewControllerDelegate <NSObject>

- (void)menuViewController:(JSMenuViewController*)menuViewController didSelectMenuItem:(JSMenuViewItem*)menuItem;
@end

@interface JSMenuViewController : UIViewController

@property (nonatomic, weak) id <JSMenuViewControllerDelegate> delegate;
@property (nonatomic, strong) NSArray* menuItems;
@property (nonatomic, strong) IBOutlet UITableView* tableView;

@end
