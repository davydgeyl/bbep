//
//  JSSecondLevelMenuViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 24/02/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSMenuViewController.h"
#import "JSMenuTableViewCell.h"

static NSString *const JSSecondLevelMenuCellItentifier = @"JSSecondLevelMenuCellItentifier";
static CGFloat const JSSecondLevelMenuMinOpacity = 0.1;
static CGFloat const JSSecondLevelMenuMaxOpacity = 0.3;

@implementation JSMenuViewItem

+ (instancetype)itemWithIdentifier:(NSString *)itemID title:(NSString *)title {
	return [self itemWithIdentifier:itemID title:title imageName:nil];
}

+ (instancetype)itemWithIdentifier:(NSString *)itemID title:(NSString *)title imageName:(NSString *)imageName {
	JSMenuViewItem *item = [[[self class] alloc] init];
	item.identifier = itemID;
	item.title = title;
	item.imageName = imageName;
	return item;
}

@end

@interface JSMenuViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation JSMenuViewController

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.tableView registerNib:[UINib nibWithNibName:@"JSMenuTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:JSSecondLevelMenuCellItentifier];
}

- (void)setMenuItems:(NSArray *)menuItems
{
    _menuItems = menuItems;
    [self.tableView reloadData];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.tableView reloadData];
}

#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	JSMenuViewItem *menuItem = self.menuItems[indexPath.row];

	JSMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:JSSecondLevelMenuCellItentifier];

	cell.caption.text = menuItem.title;
	cell.image = [UIImage imageNamed:menuItem.imageName];

	return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat cellOpacity = JSSecondLevelMenuMinOpacity;

	if (self.menuItems.count > 0) {
		cellOpacity += indexPath.row * (JSSecondLevelMenuMaxOpacity - JSSecondLevelMenuMinOpacity) / (self.menuItems.count - 1);
	}
	cell.backgroundColor = [UIColor colorWithWhite:1.0 alpha:cellOpacity];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return round(self.tableView.bounds.size.height / self.menuItems.count);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	JSMenuViewItem *menuItem = self.menuItems[indexPath.row];
	[self.delegate menuViewController:self didSelectMenuItem:menuItem];
}

@end
