//
//  Copyright (c) 2015 Jettysoft. All rights reserved.
//

#import "JSKeyValueObserver.h"

@interface JSKeyValueObserver()

@property (nonatomic, weak) id objectObserved;
@property (nonatomic, copy) NSString *keyPath;
@property (nonatomic, weak) id target;
@property (nonatomic, assign) SEL selector;
@property (nonatomic, strong) NSInvocation* invocation;

@end

@implementation JSKeyValueObserver

+ (instancetype)observerForObject:(id)object keyPath:(NSString*)keyPath target:(id)target selector:(SEL)selector __attribute__((warn_unused_result))
{
    return [[self alloc] initWithObject:object ketPath:keyPath target:target selector:(SEL)selector options:0];
}

+ (instancetype)observerForObject:(id)object keyPath:(NSString*)keyPath target:(id)target selector:(SEL)selector options:(NSKeyValueObservingOptions)options __attribute__((warn_unused_result))
{
    return [[self alloc] initWithObject:object ketPath:keyPath target:target selector:(SEL)selector options:(NSKeyValueObservingOptions)options];
}

- (instancetype)initWithObject:(id)object ketPath:(NSString*)keyPath target:(id)target selector:(SEL)selector options:(NSKeyValueObservingOptions)options

{
    NSParameterAssert(keyPath != nil);
    NSParameterAssert(target != nil);
    NSParameterAssert([target respondsToSelector:selector]);
    
    if (!object) {
        return nil;
    }
    
    self = [super init];
    if (self) {
        self.objectObserved = object;
        self.keyPath = keyPath;
        self.target = target;
        self.selector = selector;
        
        NSMethodSignature* methodSignature = [target methodSignatureForSelector:selector];
        NSInvocation* invocation = [NSInvocation invocationWithMethodSignature:methodSignature];
        invocation.target = target;
        invocation.selector = selector;
        self.invocation = invocation;
        
        [object addObserver:self forKeyPath:keyPath options:options context:nil];
    }
    return self;
}

- (void)dealloc
{
    [self.objectObserved removeObserver:self forKeyPath:self.keyPath];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (change) {
        [self.invocation setArgument:&change atIndex:2];
    }
    
    [self.invocation invoke];
}

@end
