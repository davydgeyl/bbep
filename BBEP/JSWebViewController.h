//
//  JSWebViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 13/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSViewController.h"

@interface JSWebViewController : JSViewController

@property (nonatomic, strong) NSURL* linkURL;

@end
