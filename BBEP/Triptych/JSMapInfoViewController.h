//
//  JSMapInfoViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 3/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSMapInfoViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel* titlelabel;
@property (nonatomic, strong) IBOutlet UILabel* detailsLabel;

- (void)resizeToFit;

@end
