//
//  JSMapInfoViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 3/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSMapInfoViewController.h"

@interface JSMapInfoViewController ()

@end

@implementation JSMapInfoViewController

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        [self view];
    }
    return self;
}

- (void)resizeToFit
{
    [_detailsLabel sizeToFit];
}

@end
