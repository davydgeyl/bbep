//
//  JSTriptychViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 3/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSTriptychViewController.h"
#import "JSMapView.h"

const CGFloat JSTriptychViewControllerCopyrightLabelHeight = 20.0;
const CGFloat JSTriptychViewControllerCopyrightLabelWidth = 400.0;
const CGFloat JSTriptychViewControllerCopyrightLabelHorizontalPosition = 0.63;
const CGFloat JSTriptychViewControllerCopyrightLabelBottomMargin = 5.0;

@interface JSTriptychViewController ()

@property (nonatomic, strong) UILabel* copyrightLabel;

@end

@implementation JSTriptychViewController

- (id)init
{
    self = [super init];
    if (self)
    {
        NSArray* mapRegions = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"triptychMap" ofType:@"plist"]];
        self.mapView.mapRegions = mapRegions;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Northern Wei Painting - Triptych", @"Navigation title");
    self.mapImage = [UIImage imageNamed:@"Triptych_Map.jpg"];

    CGRect bounds = self.mapView.frame;
    _copyrightLabel = [[UILabel alloc] initWithFrame:CGRectMake(round(bounds.size.width * JSTriptychViewControllerCopyrightLabelHorizontalPosition), bounds.size.height - JSTriptychViewControllerCopyrightLabelHeight - JSTriptychViewControllerCopyrightLabelBottomMargin, JSTriptychViewControllerCopyrightLabelWidth, JSTriptychViewControllerCopyrightLabelHeight)];
    _copyrightLabel.text = @"© 2012 Nancy Cowardin / IBPS Hsilai Temple";
    [self.mapView addSubview:_copyrightLabel];
}

@end
