//
//  JSTimelineMarkerLayer.m
//  BBEP
//
//  Created by Davyd Geyl on 10/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSTimelineMarkerLayer.h"
#import "JSConstants.h"

CGFloat const JSTimelineMarkerLayerCircleDiameter = 15.0;
CGFloat const JSTimelineMarkerLayerLineThickness = 3.0;

CGFloat const JSTimelineMarkerLayerCircleDiameterSmall = 10.0;
CGFloat const JSTimelineMarkerLayerLineThicknessSmall = 2.0;

@implementation JSTimelineMarkerLayer

- (id)init
{
    self = [super init];
    if (self)
    {
        self.needsDisplayOnBoundsChange = YES;
    }
    return self;
}

- (void)drawHorizontalInContext:(CGContextRef)ctx
{
    CGRect bounds = self.bounds;
    CGFloat diameter = self.diameter;
    CGFloat thickness = self.thickness;
    
    CGPoint middlePoint = CGPointMake(bounds.size.width / 2.0, bounds.size.height / 2.0);
    
    if (!_isFirst)
    {
        CGRect leftLineRect = CGRectMake(0, round(middlePoint.y - thickness / 2.0), middlePoint.x, thickness);
        CGContextAddRect(ctx, leftLineRect);
    }
    
    if (!_isLast)
    {
        CGRect rightLineRect = CGRectMake(middlePoint.x, round(middlePoint.y - thickness / 2.0), middlePoint.x, thickness);
        CGContextAddRect(ctx, rightLineRect);
    }
    
    CGRect circleRect = CGRectMake(round(middlePoint.x - diameter / 2.0), round(middlePoint.y - diameter / 2.0), diameter, diameter);
    CGContextAddEllipseInRect(ctx, circleRect);
}

- (CGFloat)diameter
{
    return _isSmall ? JSTimelineMarkerLayerCircleDiameterSmall : JSTimelineMarkerLayerCircleDiameter;
}

- (CGFloat)thickness
{
    return _isSmall ? JSTimelineMarkerLayerLineThicknessSmall : JSTimelineMarkerLayerLineThickness;
}

- (void)drawVerticalInContext:(CGContextRef)ctx
{
    CGRect bounds = self.bounds;
    
    CGFloat diameter = self.diameter;
    CGFloat thickness = self.thickness;
    
    CGPoint middlePoint = CGPointMake(bounds.size.width / 2.0, bounds.size.height / 2.0);
    
    if (!_isFirst)
    {
        CGRect leftLineRect = CGRectMake(round(middlePoint.x - thickness / 2.0), 0, thickness, _verticalMarkerOffset);
        CGContextAddRect(ctx, leftLineRect);
    }
    
    if (!_isLast)
    {
        CGRect rightLineRect = CGRectMake(round(middlePoint.x - thickness / 2.0), _verticalMarkerOffset, thickness, bounds.size.height - _verticalMarkerOffset);
        CGContextAddRect(ctx, rightLineRect);
    }
    
    CGRect circleRect = CGRectMake(round(middlePoint.x - diameter / 2.0), round(_verticalMarkerOffset - diameter / 2.0), diameter, diameter);
    CGContextAddEllipseInRect(ctx, circleRect);
}

- (void)drawInContext:(CGContextRef)ctx
{
    if (_isVertical)
        [self drawVerticalInContext:ctx];
    else
        [self drawHorizontalInContext:ctx];
    
    CGContextSetFillColorSpace(ctx, [JSConstants sharedConstants].colorSpaceDeviceRGB);
    CGFloat comps[] = {126.0/255.0, 194.0/255.0, 193.0/255.0, 1.0};
    CGContextSetFillColor(ctx, comps);
    CGContextFillPath(ctx);
    
    if (_isAccessoryMarkVisible)
    {
        CGRect bounds = self.bounds;
        CGFloat diameter = self.diameter;
        CGPoint middlePoint = CGPointMake(bounds.size.width / 2.0, bounds.size.height / 2.0);
        
        CGRect circleRect = CGRectZero;
        if (_isVertical)
            circleRect = CGRectMake(round(middlePoint.x - diameter / 2.0), round(_verticalMarkerOffset - diameter / 2.0), diameter, diameter);
        else
            circleRect = CGRectMake(round(middlePoint.x - diameter / 2.0), round(middlePoint.y - diameter / 2.0), diameter, diameter);

        CGFloat arrowHeight = 0.8;
        CGFloat arrowHorzStart = 0.35;
        CGFloat arrowHorzEnd = 0.6;
        
        CGContextMoveToPoint(ctx, round(circleRect.origin.x + diameter * arrowHorzStart), round(circleRect.origin.y + diameter * (1.0 - arrowHeight)));
        CGContextAddLineToPoint(ctx, round(circleRect.origin.x + diameter * arrowHorzEnd), round(circleRect.origin.y + circleRect.size.height / 2.0));
        CGContextAddLineToPoint(ctx, round(circleRect.origin.x + diameter * arrowHorzStart), round(circleRect.origin.y + diameter * arrowHeight));

        CGContextSetStrokeColorSpace(ctx, [JSConstants sharedConstants].colorSpaceDeviceRGB);
        CGFloat strokeComps[] = {0.22, 0.35, 1.0, 1.0};
        CGContextSetStrokeColor(ctx, strokeComps);
        CGContextSetLineWidth(ctx, 2.0);
        CGContextStrokePath(ctx);
    }
}

- (void)setIsFirst:(BOOL)isFirst
{
    _isFirst = isFirst;
    [self setNeedsDisplay];
}

- (void)setIsLast:(BOOL)isLast
{
    _isLast = isLast;
    [self setNeedsDisplay];
}

- (void)setIsAccessoryMarkVisible:(BOOL)isVisible
{
    _isAccessoryMarkVisible = isVisible;
    [self setNeedsDisplay];
}

@end
