//
//  JSTimelineCell.h
//  BBEP
//
//  Created by Davyd Geyl on 11/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSTimelineCell : UITableViewCell

@property (nonatomic, strong) NSDictionary* event;

@property (nonatomic, assign) BOOL isFirst;
@property (nonatomic, assign) BOOL isLast;

@property (nonatomic, readonly) CGFloat heightThatFits;

@property (nonatomic, readonly) NSURL* linkURL;

@end
