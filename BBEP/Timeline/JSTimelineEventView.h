//
//  JSTimelineEventView.h
//  BBEP
//
//  Created by Davyd Geyl on 7/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSTimelineEventView : UIView

@property (nonatomic, strong) NSDictionary* event;

@property (nonatomic, assign) BOOL isFirst;
@property (nonatomic, assign) BOOL isLast;

@property (nonatomic, assign) BOOL isSelected;

@end
