//
//  JSTimelineCell.m
//  BBEP
//
//  Created by Davyd Geyl on 11/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSTimelineCell.h"
#import "JSTimelineMarkerView.h"

@interface JSTimelineCell ()

@property (nonatomic, strong) IBOutlet UIView* eventContentView;
@property (nonatomic, strong) IBOutlet UIImageView* eventImageView;
@property (nonatomic, strong) IBOutlet JSTimelineMarkerView* markerView;
@property (nonatomic, strong) IBOutlet UILabel* timeLabel;
@property (nonatomic, strong) IBOutlet UILabel* titleLabel;
@property (nonatomic, strong) IBOutlet UILabel* descriptionLabel;

@end

static CGFloat _bottomDescriptionGap = 8.0;

static CGFloat _titleToDescriptionGap = 0;

@implementation JSTimelineCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [[NSBundle mainBundle] loadNibNamed:@"JSTimelineCell" owner:self options:nil];
        
        [self.contentView addSubview:_eventContentView];
        
        _titleToDescriptionGap = _descriptionLabel.frame.origin.y - _timeLabel.frame.origin.y - _timeLabel.frame.size.height;
    }
    return self;
}

- (void)setEvent:(NSDictionary*)event
{
    _event = event;
    [self update];
}

- (void)update
{
    _eventImageView.image = [UIImage imageNamed:[_event objectForKey:@"image"]];
    _titleLabel.text = [_event objectForKey:@"event"];
    _timeLabel.text = [_event objectForKey:@"time"];
    _descriptionLabel.text = [_event objectForKey:@"text"];
    
    CGSize size = [_descriptionLabel.text sizeWithFont:_descriptionLabel.font constrainedToSize:CGSizeMake(_descriptionLabel.frame.size.width, CGFLOAT_MAX) lineBreakMode:_descriptionLabel.lineBreakMode];
    
    CGRect frame = _descriptionLabel.frame;
    frame.size.height = size.height;
    _descriptionLabel.frame = frame;
    
    _heightThatFits = MAX(_descriptionLabel.frame.origin.y + _descriptionLabel.frame.size.height + _bottomDescriptionGap, _eventContentView.frame.size.height);
    
    _markerView.isVertical = YES;
    _markerView.verticalMarkerOffset = _timeLabel.frame.origin.y + ceil(_timeLabel.frame.size.height / 2.0);
    _markerView.isSmall = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone);
    
    NSString* linkURLString = [_event objectForKey:@"URL"];
    if (linkURLString != nil)
    {
        _linkURL = [NSURL URLWithString:linkURLString];
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        _linkURL = nil;
        self.accessoryType = UITableViewCellAccessoryNone;
    }
    _markerView.isAccessoryMarkVisible = (linkURLString != nil);
}

- (void)layoutSubviews
{
    CGRect markerViewFrame = _markerView.frame;
    markerViewFrame.size.height = self.bounds.size.height;
    _markerView.frame = markerViewFrame;
}

- (void)setIsLast:(BOOL)isLast
{
    _isLast = isLast;
    [_markerView setIsLast:_isLast];
}

- (void)setIsFirst:(BOOL)isFirst
{
    _isFirst = isFirst;
    [_markerView setIsFirst:isFirst];
}

@end
