//
//  JSTimelineMarkerLayer.h
//  BBEP
//
//  Created by Davyd Geyl on 10/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface JSTimelineMarkerLayer : CALayer

@property (nonatomic, assign) BOOL isFirst;
@property (nonatomic, assign) BOOL isLast;

@property (nonatomic, assign) BOOL isVertical;
@property (nonatomic, assign) CGFloat verticalMarkerOffset;

@property (nonatomic, assign) BOOL isSmall;

@property (nonatomic, assign) BOOL isAccessoryMarkVisible;

@end
