//
//  JSLandscapeTimelineView.m
//  BBEP
//
//  Created by Davyd Geyl on 7/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSLandscapeTimelineView.h"
#import "JSTimelineEventView.h"
#import "JSConstants.h"

CGFloat const JSTimelineViewEventWidthPad = 216.0;
CGFloat const JSTimelineViewEventWidth = 160.0;

@interface JSLandscapeTimelineView () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSMutableArray* timelineEvents;
@property (nonatomic, strong) UITapGestureRecognizer* tapGestureRecognizer;

@end

@implementation JSLandscapeTimelineView

@dynamic delegate;

- (void)commonInit
{
    _timelineEvents = [NSMutableArray new];

    _tapGestureRecognizer = [UITapGestureRecognizer new];
    _tapGestureRecognizer.delegate = self;
    [_tapGestureRecognizer addTarget:self action:@selector(tap:)];
    [self addGestureRecognizer:_tapGestureRecognizer];

    self.showsVerticalScrollIndicator = NO;
    self.showsHorizontalScrollIndicator = YES;
    self.alwaysBounceHorizontal = YES;
    self.alwaysBounceVertical = NO;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (void)setTimeline:(NSArray*)timeline
{
    [self clean];

    _timeline = timeline;

    [self reloadData];
}

- (void)clean
{
    for (JSTimelineEventView* timelineEvent in _timelineEvents)
        [timelineEvent removeFromSuperview];

    [_timelineEvents removeAllObjects];
}

- (CGFloat)eventWidth
{
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) ? JSTimelineViewEventWidth : JSTimelineViewEventWidthPad;
}

- (void)reloadData
{
    for (NSDictionary* event in _timeline)
    {
        JSTimelineEventView* timelineEvent = [JSTimelineEventView new];
        timelineEvent.event = event;

        [self addSubview:timelineEvent];
        [_timelineEvents addObject:timelineEvent];
    }

    if (_timelineEvents.count > 0)
        [(JSTimelineEventView*)[_timelineEvents objectAtIndex:0] setIsFirst:YES];
    [(JSTimelineEventView*)[_timelineEvents lastObject] setIsLast:YES];

    self.contentSize = CGSizeMake(_timelineEvents.count * self.eventWidth, self.bounds.size.height);

    [self setNeedsLayout];
}

- (void)layoutSubviews
{
    self.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.contentOffset = CGPointMake(self.contentOffset.x, 0);
    CGFloat contentHeight = self.bounds.size.height;

    self.contentSize = CGSizeMake(_timelineEvents.count * self.eventWidth, contentHeight);

    CGFloat height = contentHeight;

    for (JSTimelineEventView* timelineEvent in _timelineEvents)
    {
        [timelineEvent setFrame:CGRectMake([_timelineEvents indexOfObject:timelineEvent] * self.eventWidth, 0, self.eventWidth, height)];
    }
    
    [self layoutFooterView];
}

- (void)layoutFooterView
{
    CGSize contentSize = self.contentSize;
    CGSize footerViewSize = _footerView.frame.size;
    _footerView.frame = CGRectMake(contentSize.width - footerViewSize.width, contentSize.height - footerViewSize.height, footerViewSize.width, footerViewSize.height);
}

- (JSTimelineEventView*)visualTimelineEventViewAtPoint:(CGPoint)point
{
    JSTimelineEventView* result = nil;

    int index = point.x / self.eventWidth;
    if (index < _timelineEvents.count)
        result = [_timelineEvents objectAtIndex:index];

    return result;
}

- (void)tap:(UITapGestureRecognizer*)gestureRecognizer
{
    CGPoint point = [gestureRecognizer locationInView:self];
    int index = point.x / self.eventWidth;

    JSTimelineEventView* hitTimelineView = [self visualTimelineEventViewAtPoint:point];
    [hitTimelineView setIsSelected:YES];

    if (index >= 0 && index < _timelineEvents.count)
    {
        [self.delegate landscapeTimelineView:self didSelectEventAtIndex:index];
    }
}

- (void)deselectEventAtIndex:(NSUInteger)index animated:(BOOL)animated
{
    JSTimelineEventView* timelineView = [_timelineEvents objectAtIndex:index];
    [timelineView setIsSelected:NO];
}

- (void)setFooterView:(UIView *)footerView
{
    _footerView = footerView;
    [self addSubview:_footerView];
    [self layoutFooterView];
}

@end
