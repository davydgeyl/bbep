//
//  JSTimelineMarkerView.m
//  BBEP
//
//  Created by Davyd Geyl on 10/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSTimelineMarkerView.h"
#import "JSTimelineMarkerLayer.h"

@implementation JSTimelineMarkerView

+ (Class)layerClass
{
    return [JSTimelineMarkerLayer class];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.layer.contentsScale = [[UIScreen mainScreen] scale];
}

- (BOOL)isFirst
{
    return [(JSTimelineMarkerLayer*)self.layer isFirst];
}

- (void)setIsFirst:(BOOL)isFirst
{
    [(JSTimelineMarkerLayer*)self.layer setIsFirst : isFirst];
}

- (BOOL)isLast
{
    return [(JSTimelineMarkerLayer*)self.layer isLast];
}

- (void)setIsLast:(BOOL)isLast
{
    [(JSTimelineMarkerLayer*)self.layer setIsLast : isLast];
}

- (BOOL)isVertical
{
    return [(JSTimelineMarkerLayer*)self.layer isVertical];
}

- (void)setIsVertical:(BOOL)isVertical
{
    [(JSTimelineMarkerLayer*)self.layer setIsVertical : isVertical];
}

- (CGFloat)verticalMarkerOffset
{
    return [(JSTimelineMarkerLayer*)self.layer verticalMarkerOffset];
}

- (void)setVerticalMarkerOffset:(CGFloat)verticalMarkerOffset
{
    [(JSTimelineMarkerLayer*)self.layer setVerticalMarkerOffset : verticalMarkerOffset];
}

- (BOOL)isSmall
{
    return [(JSTimelineMarkerLayer*)self.layer isSmall];
}

- (void)setIsSmall:(BOOL)isSmall
{
    [(JSTimelineMarkerLayer*)self.layer setIsSmall : isSmall];
}

- (BOOL)isAccessoryMarkVisible
{
    return [(JSTimelineMarkerLayer*)self.layer isAccessoryMarkVisible];
}

- (void)setIsAccessoryMarkVisible:(BOOL)isVisible
{
    [(JSTimelineMarkerLayer*)self.layer setIsAccessoryMarkVisible : isVisible];
}

@end
