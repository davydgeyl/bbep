//
//  JSTimelineEventView.m
//  BBEP
//
//  Created by Davyd Geyl on 7/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSTimelineEventView.h"
#import "JSTimelineMarkerView.h"

@interface JSTimelineEventView ()

@property (nonatomic, strong) IBOutlet UIView* contentView;
@property (nonatomic, strong) IBOutlet UIImageView* imageView;
@property (nonatomic, strong) IBOutlet JSTimelineMarkerView* markerView;
@property (nonatomic, strong) IBOutlet UILabel* timeLabel;
@property (nonatomic, strong) IBOutlet UILabel* titleLabel;
@property (nonatomic, strong) IBOutlet UILabel* descriptionLabel;

@end

static CGFloat _titleToDescriptionGap = 0;

@implementation JSTimelineEventView

- (id)init
{
    self = [super init];
    if (self)
    {
        [[NSBundle mainBundle] loadNibNamed:@"TimelineEventView" owner:self options:nil];

        [self addSubview:_contentView];
        _titleToDescriptionGap = _descriptionLabel.frame.origin.y - _titleLabel.frame.origin.y - _titleLabel.frame.size.height;
    }
    return self;
}

- (void)setEvent:(NSDictionary*)event
{
    _event = event;
    [self update];
}

- (void)update
{
    _imageView.image = [UIImage imageNamed:[_event objectForKey:@"image"]];
    _titleLabel.text = [_event objectForKey:@"event"];
    _timeLabel.text = [_event objectForKey:@"time"];
    _descriptionLabel.text = [_event objectForKey:@"text"];
    
    [_titleLabel sizeToFit];
    [_descriptionLabel sizeToFit];
    
    _markerView.isSmall = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone);
    
    NSString* linkURLString = [_event objectForKey:@"URL"];
    _markerView.isAccessoryMarkVisible = (linkURLString != nil);
    
    [self setNeedsLayout];
}

- (void)setIsLast:(BOOL)isLast
{
    _isLast = isLast;
    [_markerView setIsLast:_isLast];
}

- (void)setIsFirst:(BOOL)isFirst
{
    _isFirst = isFirst;
    [_markerView setIsFirst:isFirst];
}

- (void)layoutSubviews
{
    _contentView.frame = self.bounds;
    
    _descriptionLabel.frame = CGRectMake(_descriptionLabel.frame.origin.x, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + _titleToDescriptionGap, _descriptionLabel.frame.size.width, _descriptionLabel.frame.size.height);
}

- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    [self updateSelection];
}

- (void)updateSelection
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.7];
    if (_isSelected)
        self.backgroundColor = [UIColor colorWithHue:210.0 / 360.0 saturation:0.18 brightness:1.0 alpha:1.0];
    else
        self.backgroundColor = [UIColor clearColor];
    
    [UIView commitAnimations];
}

@end
