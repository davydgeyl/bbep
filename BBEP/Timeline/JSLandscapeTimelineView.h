//
//  JSLandscapeTimelineView.h
//  BBEP
//
//  Created by Davyd Geyl on 7/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JSLandscapeTimelineView;

@protocol JSLandscapeTimelineViewDelegate <UIScrollViewDelegate>

- (void)landscapeTimelineView:(JSLandscapeTimelineView*)view didSelectEventAtIndex:(NSUInteger)index;

@end

@interface JSLandscapeTimelineView : UIScrollView

@property (nonatomic, strong) NSArray* timeline;
@property (nonatomic, weak) id <JSLandscapeTimelineViewDelegate> delegate;

- (void)deselectEventAtIndex:(NSUInteger)index animated:(BOOL)animated;

@property (nonatomic, strong) UIView* footerView;

@end
