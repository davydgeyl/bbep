//
//  JSTimelineViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 6/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSTimelineViewController.h"
#import "JSLandscapeTimelineView.h"
#import "JSTimelineCell.h"
#import "JSWebViewController.h"
#import "JSConstants.h"

CGFloat const JSViewControllerStatusBarHeight = 20.0;
NSString* const JSTimelineViewControllerCellIdentifier = @"cell";

@interface JSTimelineViewController () <UITableViewDataSource, UITableViewDelegate, JSLandscapeTimelineViewDelegate>

@property (nonatomic, strong) NSArray* timelineEvents;
@property (nonatomic, strong) IBOutlet JSLandscapeTimelineView* landscapeTimelineView;
@property (nonatomic, strong) IBOutlet UITableView* portraitTimelineView;
@property (nonatomic, strong) IBOutlet UIView* portraitFooterView;
@property (nonatomic, strong) IBOutlet UIView* landscapeFooterView;

@end

@implementation JSTimelineViewController

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        _timelineEvents = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"timeline" ofType:@"plist"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.title = NSLocalizedString(@"Historical Journey", @"Navigation title");

    _landscapeTimelineView.timeline = _timelineEvents;
    _landscapeTimelineView.delegate = self;
    _landscapeTimelineView.footerView = _landscapeFooterView;

    _portraitTimelineView.tableFooterView = _portraitFooterView;

    [self updateViews];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateViews];
}

- (void)updateViews
{
    CGRect bounds = self.view.bounds;

    UIInterfaceOrientation interfaceOrientation = self.interfaceOrientation;
    if ((interfaceOrientation == UIInterfaceOrientationPortrait) || (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown))
    {
        [_landscapeTimelineView removeFromSuperview];
        _portraitTimelineView.frame = bounds;
        [_portraitTimelineView reloadData];
        [self.view addSubview:_portraitTimelineView];
    }
    else
    {
        [_portraitTimelineView removeFromSuperview];

        CGFloat titleHeight = [[JSConstants sharedConstants] titleBarHeightForInterfaceOrientation:self.interfaceOrientation];

        _landscapeTimelineView.frame = CGRectMake(0, titleHeight, bounds.size.width, bounds.size.height - titleHeight);
        _landscapeTimelineView.contentInset = UIEdgeInsetsMake(titleHeight, 0, 0, 0);
        _landscapeTimelineView.contentOffset = CGPointZero;
        [self.view addSubview:_landscapeTimelineView];
    }
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_timelineEvents count];
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    JSTimelineCell* cell = [tableView dequeueReusableCellWithIdentifier:JSTimelineViewControllerCellIdentifier];
    if (cell == nil)
    {
        cell = [[JSTimelineCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:JSTimelineViewControllerCellIdentifier];
    }

    NSDictionary* event = [_timelineEvents objectAtIndex:indexPath.row];

    cell.event = event;
    cell.isFirst = (indexPath.row == 0);
    cell.isLast = (indexPath.row == _timelineEvents.count - 1);

    return cell;
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    JSTimelineCell* cell = [[JSTimelineCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    NSDictionary* event = [_timelineEvents objectAtIndex:indexPath.row];
    cell.event = event;

    return cell.heightThatFits;
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSDictionary* event = [_timelineEvents objectAtIndex:indexPath.row];
    [self presentWebViewForEvent:event];
}

- (void)presentWebViewForEvent:(NSDictionary*)event
{
    NSString* linkURLString = [event objectForKey:@"URL"];
    if (linkURLString != nil)
    {
        NSURL* linkURL = [NSURL URLWithString:linkURLString];

        JSWebViewController* webViewController = [JSWebViewController new];
        webViewController.linkURL = linkURL;
        [self.navigationController pushViewController:webViewController animated:YES];
    }
}

#pragma mark JSLandscapeTimelineViewDelegate

- (void)landscapeTimelineView:(JSLandscapeTimelineView*)view didSelectEventAtIndex:(NSUInteger)index
{
    [view deselectEventAtIndex:index animated:YES];

    NSDictionary* event = [_timelineEvents objectAtIndex:index];
    [self presentWebViewForEvent:event];
}

@end
