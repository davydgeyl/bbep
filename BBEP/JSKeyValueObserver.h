//
//  Copyright (c) 2015 Jettysoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSKeyValueObserver : NSObject

+ (instancetype)observerForObject:(id)object keyPath:(NSString*)keyPath target:(id)target selector:(SEL)selector;

+ (instancetype)observerForObject:(id)object keyPath:(NSString*)keyPath target:(id)target selector:(SEL)selector options:(NSKeyValueObservingOptions)options __attribute__
        ((warn_unused_result));

@end
