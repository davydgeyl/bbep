//
//  JSDharmaCardDataModel.h
//  BBEP
//
//  Created by Davyd Geyl on 4/06/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const JSDharmaCardDataModelDidChangeNotification;
extern NSString* const JSDharmaCardImageNameFormat;

@interface JSDharmaCardDataModel : NSObject

+ (JSDharmaCardDataModel*)sharedDataModel;

@property (nonatomic, assign) NSUInteger currentCardNumber;

- (void)changeToNextCard;

@property (nonatomic, readonly) BOOL isScheduled;
@property (nonatomic, strong) NSDate* scheduledDate;

@property (nonatomic, assign) BOOL shouldShowCardUponLaunch;

@end
