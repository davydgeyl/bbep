//
//  JSDharmaCardsViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 29/05/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSViewController.h"

@interface JSDharmaCardsViewController : JSViewController

- (IBAction)getCard:(id)sender;

@end
