//
//  JSDharmaCardsViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 29/05/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSDharmaCardsViewController.h"
#import "JSDharmaCardsScheduleViewController.h"
#import "JSDharmaCardView.h"
#import "JSConstants.h"
#import "JSDharmaCardDataModel.h"
#import <MessageUI/MessageUI.h>

@interface JSDharmaCardsViewController () <MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) IBOutlet JSDharmaCardView* dharmaCardView;

@end

@implementation JSDharmaCardsViewController

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem* scheduleButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Schedule", @"Button title") style:UIBarButtonItemStylePlain target:self action:@selector(scheduleCards)];

    UIBarButtonItem* shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareCard:)];

    self.navigationItem.rightBarButtonItems = @[scheduleButton, shareButton];

    if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0)
    {
        scheduleButton.tintColor = [UIColor colorWithRed:0.4 green:0.65 blue:1.0 alpha:1.0];
        shareButton.tintColor = [UIColor colorWithRed:0.4 green:0.65 blue:1.0 alpha:1.0];
    }

    _dharmaCardView.dataModel = [JSDharmaCardDataModel sharedDataModel];

    [self updateViews];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateViews];
}

- (void)updateViews
{
    CGRect bounds = self.view.bounds;
    CGFloat bottomMargin = bounds.size.height - _dharmaCardView.frame.size.height - _dharmaCardView.frame.origin.y;
    CGFloat titleHeight = [[JSConstants sharedConstants] titleBarHeightForInterfaceOrientation:self.interfaceOrientation];

    UIInterfaceOrientation interfaceOrientation = self.interfaceOrientation;
    if ((interfaceOrientation == UIInterfaceOrientationPortrait) || (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown))
    {
        _dharmaCardView.frame = CGRectMake(0, titleHeight, bounds.size.width, bounds.size.height - titleHeight - bottomMargin);
    }
}

- (void)scheduleCards
{
    JSDharmaCardsScheduleViewController* dharmaCardsScheduleViewController = [JSDharmaCardsScheduleViewController new];

    dharmaCardsScheduleViewController.dataModel = [JSDharmaCardDataModel sharedDataModel];
    [self presentViewController:dharmaCardsScheduleViewController animated:YES completion:nil];
}

- (IBAction)getCard:(id)sender
{
    [[JSDharmaCardDataModel sharedDataModel] changeToNextCard];
}

- (void)shareCard:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController* controller = [MFMailComposeViewController new];
        controller.mailComposeDelegate = self;
        [controller setSubject:NSLocalizedString(@"BBEP Dharma Card", @"Mail subject")];

        NSString* fileName = [NSString stringWithFormat:JSDharmaCardImageNameFormat, [[JSDharmaCardDataModel sharedDataModel] currentCardNumber]];
        NSData* imageData = UIImageJPEGRepresentation([UIImage imageNamed:fileName], 0.8);
        [controller addAttachmentData:imageData mimeType:@"image/jpeg" fileName:fileName];

        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Default Mail Account Required", @"Alert title") message:NSLocalizedString(@"Please setup a Default Account in the Mail Settings", @"Alert message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Button") otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
