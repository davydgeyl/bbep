//
//  JSDharmaCardsScheduleViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 29/05/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JSDharmaCardsScheduleViewController;

@protocol JSDharmaCardsScheduleViewControllerDelegate <NSObject>

- (void)dharmaCardsScheduleViewControllerDone:(JSDharmaCardsScheduleViewController*)controller;

@end

@class JSDharmaCardDataModel;

@interface JSDharmaCardsScheduleViewController : UIViewController

@property (nonatomic, weak) id <JSDharmaCardsScheduleViewControllerDelegate> delegate;
@property (nonatomic, strong) IBOutlet UISwitch* scheduleSwitch;
@property (nonatomic, strong) IBOutlet UIDatePicker* timePicker;

- (IBAction)changeScheduling:(id)sender;
- (IBAction)changeDate:(id)sender;
- (IBAction)done:(id)sender;

@property (nonatomic, strong) JSDharmaCardDataModel* dataModel;

@end
