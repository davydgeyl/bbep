//
//  JSDharmaCardDataModel.m
//  BBEP
//
//  Created by Davyd Geyl on 4/06/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSDharmaCardDataModel.h"

static JSDharmaCardDataModel* _sharedDataModel = nil;

static NSString* const JSDharmaCardCurrentNumberKey = @"JSDharmaCardCurrentNumberKey";
static NSString* const JSDharmaCardScheduledDateKey = @"JSDharmaCardScheduledDateKey";

NSString* const JSDharmaCardDataModelDidChangeNotification = @"JSDharmaCardDataModelDidChangeNotification";

NSUInteger const JSDharmaCardsNumber = 52;
NSString* const JSDharmaCardImageNameFormat = @"dharma%02d_e.jpeg";

@implementation JSDharmaCardDataModel

+ (JSDharmaCardDataModel*)sharedDataModel
{
    if (_sharedDataModel == nil)
        _sharedDataModel = [JSDharmaCardDataModel new];

    return _sharedDataModel;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self restore];

        NSInteger cardNumber = [[NSUserDefaults standardUserDefaults] integerForKey:JSDharmaCardCurrentNumberKey];
        if (cardNumber <= 0)
            cardNumber = 1;
        self.currentCardNumber = cardNumber;

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    }
    return self;
}

- (BOOL)isScheduled
{
    return _scheduledDate != nil;
}

- (void)setScheduledDate:(NSDate*)scheduledDate
{
    _scheduledDate = scheduledDate;
    [[NSNotificationCenter defaultCenter] postNotificationName:JSDharmaCardDataModelDidChangeNotification object:self];
    [self save];
    [self updateLocalNotifications];
}

- (void)setCurrentCardNumber:(NSUInteger)currentCardNumber
{
    _currentCardNumber = currentCardNumber;

    [[NSNotificationCenter defaultCenter] postNotificationName:JSDharmaCardDataModelDidChangeNotification object:self];

    [self save];
}

- (void)changeToNextCard
{
    NSUInteger nextCardNumber = _currentCardNumber + 1;
    if (nextCardNumber > JSDharmaCardsNumber)
        nextCardNumber = 1;

    self.currentCardNumber = nextCardNumber;
}

- (void)save
{
    [[NSUserDefaults standardUserDefaults] setInteger:_currentCardNumber forKey:JSDharmaCardCurrentNumberKey];
    [[NSUserDefaults standardUserDefaults] setObject:_scheduledDate forKey:JSDharmaCardScheduledDateKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)restore
{
    _currentCardNumber = [[NSUserDefaults standardUserDefaults] integerForKey:JSDharmaCardCurrentNumberKey];
    _scheduledDate = [[NSUserDefaults standardUserDefaults] objectForKey:JSDharmaCardScheduledDateKey];
}

- (void)registerForRemoteNotification {
    if ([[UIDevice currentDevice] systemVersion].floatValue >= 8.0) {
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
}

- (void)updateLocalNotifications
{
    [self registerForRemoteNotification];
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];

    if (_scheduledDate != nil)
    {
        UILocalNotification* localNotification  = [UILocalNotification new];
        localNotification.applicationIconBadgeNumber = 1;
        localNotification.timeZone = [NSTimeZone localTimeZone];
        localNotification.repeatInterval = NSCalendarUnitDay;
        localNotification.fireDate = _scheduledDate;

        localNotification.alertAction = @"Daily Dharma Card";
        localNotification.alertBody = @"Daily Dharma Card";
        localNotification.soundName = UILocalNotificationDefaultSoundName;

        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}

- (void)applicationDidBecomeActive
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)setShouldShowCardUponLaunch:(BOOL)shouldShowCardUponLaunch
{
    _shouldShowCardUponLaunch = shouldShowCardUponLaunch;

    if (_shouldShowCardUponLaunch)
        [self changeToNextCard];
}

@end
