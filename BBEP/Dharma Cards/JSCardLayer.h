//
//  JSCardLayer.h
//  BBEP
//
//  Created by Davyd Geyl on 31/05/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface JSCardLayer : CALayer

- (instancetype)initWithImageName:(NSString*)imageName layerScale:(CGFloat)layerScale;

@property (nonatomic, readonly) CGFloat layerScale;
@property (nonatomic, strong) NSString* imageName;

@end
