//
//  JSCardLayer.m
//  BBEP
//
//  Created by Davyd Geyl on 31/05/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSCardLayer.h"


@interface JSCardLayer ()
{
    CALayer* _imageLayer;
}

@property (nonatomic, assign) CGFloat layerScale;

@end

@implementation JSCardLayer

- (instancetype)init
{
    return [self initWithImageName:nil layerScale:1.0];
}

- (instancetype)initWithImageName:(NSString*)imageName layerScale:(CGFloat)layerScale
{
    self = [super init];
    if (self)
    {
        _imageLayer = [CALayer new];
        _layerScale = layerScale;

        [self setImageName:imageName];

        self.shadowColor = [[UIColor blackColor] CGColor];
        self.shadowRadius = 5.0;
        self.shadowOpacity = 0.75;
        self.shadowOffset = CGSizeMake(0, 2.0);
        self.cornerRadius = 20.0;
        self.allowsEdgeAntialiasing = YES;

        [self addSublayer:_imageLayer];
    }
    return self;
}

- (void)setImageName:(NSString *)imageName
{
    UIImage* image = [UIImage imageNamed:imageName];
    
    CGImageRef imageRef = [image CGImage];
    _imageLayer.contents = (__bridge id)imageRef;
    _imageLayer.frame = CGRectMake(0, 0, image.size.width * self.layerScale, image.size.height * self.layerScale);
    _imageLayer.masksToBounds = YES;
    _imageLayer.cornerRadius = 20.0;
    _imageLayer.allowsEdgeAntialiasing = YES;
    
    self.bounds = CGRectMake(0, 0, image.size.width * self.layerScale, image.size.height * self.layerScale);
}

@end
