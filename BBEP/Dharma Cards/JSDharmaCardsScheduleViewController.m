//
//  JSDharmaCardsScheduleViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 29/05/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSDharmaCardsScheduleViewController.h"
#import "JSDharmaCardDataModel.h"

@interface JSDharmaCardsScheduleViewController ()

@end

@implementation JSDharmaCardsScheduleViewController

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self update];
    [self updateLocal];
}

- (IBAction)changeScheduling:(id)sender
{
    [self updateLocal];
}

- (void)updateLocal
{
    _timePicker.hidden = !_scheduleSwitch.isOn;
}

- (void)update
{
    _scheduleSwitch.on = (_dataModel.scheduledDate != nil);
    
    NSDate* date = _dataModel.scheduledDate;
    if (date != nil)
        _timePicker.date = date;
}

- (IBAction)changeDate:(id)sender
{

}

- (void)setDataModel:(JSDharmaCardDataModel*)dataModel
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:JSDharmaCardDataModelDidChangeNotification object:nil];

    _dataModel = dataModel;

    if (_dataModel != nil)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataModelDidChange) name:JSDharmaCardDataModelDidChangeNotification object:_dataModel];
    }

    [self update];
}

- (void)dataModelDidChange
{
    [self update];
}

- (IBAction)done:(id)sender
{
    _dataModel.scheduledDate = _scheduleSwitch.isOn ? _timePicker.date : nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
