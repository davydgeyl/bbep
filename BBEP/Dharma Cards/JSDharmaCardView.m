//
//  JSDharmaCardView.m
//  BBEP
//
//  Created by Davyd Geyl on 31/05/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSDharmaCardView.h"
#import "JSCardLayer.h"
#import "JSDharmaCardDataModel.h"

static CGFloat const JSDharmaCardLayerScale = 0.85;

@interface JSDharmaCardView ()

@property (nonatomic, strong) JSCardLayer* currentCardLayer;

@end

@implementation JSDharmaCardView

- (void)commonInit
{
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setDataModel:(JSDharmaCardDataModel*)dataModel
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:JSDharmaCardDataModelDidChangeNotification object:nil];

    _dataModel = dataModel;

    if (_dataModel != nil)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataModelDidChange) name:JSDharmaCardDataModelDidChangeNotification object:_dataModel];
    }

    [self dataModelDidChange];
}

- (void)dataModelDidChange
{
    [self displayCardWithNumber:_dataModel.currentCardNumber];
}

- (void)displayCardWithNumber:(NSUInteger)cardNumber
{
    NSString* imageName = [NSString stringWithFormat:JSDharmaCardImageNameFormat, cardNumber];
    self.currentCardLayer = [[JSCardLayer alloc] initWithImageName:imageName layerScale:JSDharmaCardLayerScale];
}

- (void)setCurrentCardLayer:(JSCardLayer*)currentCardLayer
{
    JSCardLayer* previousLayer = _currentCardLayer;

    currentCardLayer.hidden = YES;
    _currentCardLayer = currentCardLayer;
    [self layoutCurrentLayer];
    [self.layer addSublayer:_currentCardLayer];

    [UIView animateWithDuration:2.0 animations:^
    {
        previousLayer.hidden = YES;
    }

                     completion:^(BOOL finished)
    {
        [previousLayer removeFromSuperlayer];
        [CATransaction begin];
        [CATransaction setAnimationDuration:4.0];
        _currentCardLayer.hidden = NO;
        [CATransaction commit];
    }];
}

- (void)layoutSubviews
{
    [self layoutCurrentLayer];
}

- (void)layoutCurrentLayer
{
    _currentCardLayer.position = CGPointMake(round(self.bounds.size.width / 2.0), round(self.bounds.size.height / 2.0));
}

@end
