//
//  JSCarriageSlideViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 20/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSCarriageSlideViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIScrollView* scrollView;
@property (strong, nonatomic) IBOutlet UIImageView* imageView;
@property (strong, nonatomic) IBOutlet UIView* textView;
@property (strong, nonatomic) IBOutlet UILabel* textLabel;

@property (nonatomic, strong) NSDictionary* slideDictionary;

@end
