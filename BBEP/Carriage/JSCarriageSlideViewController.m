//
//  JSCarriageSlideViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 20/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSCarriageSlideViewController.h"

static CGFloat _textToImageGap = 0;

@interface JSCarriageSlideViewController ()

@end

@implementation JSCarriageSlideViewController

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _textToImageGap = _textView.frame.origin.y - _imageView.frame.origin.y - _imageView.frame.size.height;
    _imageView.layer.shadowColor = [[UIColor blackColor] CGColor];
    _imageView.layer.shadowRadius = 5.0;
    _imageView.layer.shadowOpacity = 0.75;
    [self update];
}

- (void)setSlideDictionary:(NSDictionary*)slideDictionary
{
    _slideDictionary = slideDictionary;

    [self update];
}

- (void)update
{
    _imageView.image = [UIImage imageNamed:[_slideDictionary objectForKey:@"image"]];
    _textLabel.text = [_slideDictionary objectForKey:@"text"];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CGSize boundsSize = self.view.bounds.size;
    CGSize imageSize = _imageView.image.size;

    CGFloat imageFrameHeight = round(boundsSize.width * imageSize.height / imageSize.width);
    _imageView.frame = CGRectMake(0, 0, boundsSize.width, MIN(imageFrameHeight, boundsSize.height));


    [_textLabel sizeToFit];

    _textView.frame = CGRectMake(_textView.frame.origin.x, _imageView.frame.size.height + _textToImageGap, _textView.bounds.size.width, _textLabel.frame.origin.y + _textLabel.frame.size.height);

    CGSize contentSize = CGSizeMake(_scrollView.frame.size.width, MAX(_scrollView.bounds.size.height, _textView.frame.origin.y + _textView.frame.size.height));

    [_scrollView setContentSize:contentSize];

    _scrollView.bounces = _scrollView.contentSize.height > _scrollView.bounds.size.height;
}

@end
