//
//  JSCarriageViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 20/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSViewController.h"

@interface JSCarriageViewController : JSViewController

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem* slideCountItem;

- (IBAction)previousSlide:(id)sender;
- (IBAction)nextSlide:(id)sender;

@end
