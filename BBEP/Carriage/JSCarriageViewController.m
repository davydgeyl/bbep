//
//  JSCarriageViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 20/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSCarriageViewController.h"
#import "JSCarriageSlideViewController.h"
#import "JSConstants.h"

@interface JSCarriageViewController () <UIScrollViewDelegate>

@property (nonatomic, strong) NSArray* carriageSlides;
@property (nonatomic, strong) NSMutableArray* carriageSlideViewControllers;
@property (nonatomic, strong) UITapGestureRecognizer* tapGestureRecognizer;
@property (nonatomic, assign) BOOL isFullView;

@end

@implementation JSCarriageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        _carriageSlideViewControllers = [NSMutableArray new];
        self.carriageSlides = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"carriage" ofType:@"plist"]];
        
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
        
        self.navigationItem.title = NSLocalizedString(@"Later Zhao Carriage Design", @"Button title");
    }
    return self;
}

- (void)dealloc
{
    _scrollView.delegate = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self update];
    [_scrollView addGestureRecognizer:_tapGestureRecognizer];
    [_toolbar setTintColor:[JSConstants sharedConstants].toolbarTintColor];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateScrollView];
    [self layoutSlides];
}

- (void)setCarriageSlides:(NSArray *)carriageSlides
{
    _carriageSlides = carriageSlides;
    [self update];
}

- (void)clean
{
    for (JSCarriageSlideViewController* controller in _carriageSlideViewControllers)
    {
        [controller.view removeFromSuperview];
    }
    
    [_carriageSlideViewControllers removeAllObjects];
}

- (void)update
{
    [self clean];
    
    if (_scrollView != nil)
    {
        for (NSDictionary* carriageSlide in _carriageSlides)
        {            
            JSCarriageSlideViewController* controller = [JSCarriageSlideViewController new];
            controller.slideDictionary = carriageSlide;
            [_carriageSlideViewControllers addObject:controller];
            [_scrollView addSubview:controller.view];
        }
        [self updateScrollView];
    }
    [self updateCountItem];
}

- (void)updateScrollView
{
    CGRect bounds = self.view.bounds;
    CGFloat titleHeight = [[JSConstants sharedConstants] titleBarHeightForInterfaceOrientation:self.interfaceOrientation];
    
    if (self.navigationController.isNavigationBarHidden)
        titleHeight = 20;
    
    CGFloat toolbarHeight = [_toolbar isHidden] ? 0 : _toolbar.frame.size.height;
    
    [_scrollView setFrame:CGRectMake(0, titleHeight, bounds.size.width, bounds.size.height - titleHeight - toolbarHeight)];
    [_scrollView setContentSize:CGSizeMake(_carriageSlides.count * self.view.bounds.size.width, bounds.size.height - titleHeight - toolbarHeight)];
    
    _scrollView.contentInset = UIEdgeInsetsZero;
    _scrollView.contentOffset = CGPointMake(_scrollView.contentOffset.x, 0);
}

- (void)updateCountItem
{
    int currentSlidePosition = 1 + (_scrollView.contentOffset.x + _scrollView.bounds.size.width / 2.0) / _scrollView.bounds.size.width;
    _slideCountItem.title = [NSString stringWithFormat:@"%d of %lu", currentSlidePosition, (unsigned long)_carriageSlides.count];
}

- (void)layoutSlides
{
    CGRect bounds = _scrollView.bounds;

    CGFloat contentHeight = _scrollView.contentSize.height;
    for (int i = 0; i < _carriageSlideViewControllers.count; i++)
    {
        JSCarriageSlideViewController* controller = _carriageSlideViewControllers[i];
        [controller.view setFrame:CGRectMake(i * bounds.size.width, 0, bounds.size.width, contentHeight)];
    }
}

- (IBAction)previousSlide:(id)sender
{
    CGSize boundsSize = _scrollView.bounds.size;
    CGFloat nextX = MAX(_scrollView.contentOffset.x - boundsSize.width, 0);
    
    [_scrollView scrollRectToVisible:CGRectMake(nextX, 0, boundsSize.width, boundsSize.height) animated:YES];
}

- (IBAction)nextSlide:(id)sender
{
    CGSize boundsSize = _scrollView.bounds.size;
    CGFloat nextX = MIN(_scrollView.contentOffset.x + boundsSize.width, _scrollView.contentSize.width - boundsSize.width);
    
    [_scrollView scrollRectToVisible:CGRectMake(nextX, 0, boundsSize.width, boundsSize.height) animated:YES];
}

#pragma mark -

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateCountItem];
}

#pragma mark -

- (void)singleTap:(UITapGestureRecognizer*)gestureRecognizer
{
    _isFullView = !_isFullView;
    [self updateFullView];
}

- (void)updateFullView
{
    [self.navigationController setNavigationBarHidden:_isFullView animated:YES];
    [_toolbar setHidden:_isFullView];
    
    [self updateScrollView];
}

@end
