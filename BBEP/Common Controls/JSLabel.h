//
//  JSLabel.h
//  BBEP
//
//  Created by Davyd Geyl on 17/09/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSLabel : UILabel

@end
