//
//  JSImageView.m
//  BBEP
//
//  Created by Davyd Geyl on 28/02/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSImageView.h"

@implementation JSImageView

- (CGSize)intrinsicContentSize {
    return CGSizeMake(self.frame.size.width, self.explicitHeight);
}

- (void)setExplicitHeight:(CGFloat)explicitHeight
{
    _explicitHeight = explicitHeight;
    [self invalidateIntrinsicContentSize];
}

@end
