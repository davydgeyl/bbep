//
//  JSActionCellBackgroundView.m
//  BBEP
//
//  Created by Davyd Geyl on 18/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSActionCellBackgroundView.h"

@interface JSActionCellBackgroundView ()

@property (nonatomic, strong) CALayer* contentLayer;

@end

@implementation JSActionCellBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _contentLayer = [CALayer new];
        _contentLayer.cornerRadius = 8.0;
        _contentLayer.shadowOpacity = 0.5;
        _contentLayer.shadowColor = [[UIColor darkGrayColor] CGColor];
        _contentLayer.shadowOffset = CGSizeMake(0, 0);
        _contentLayer.shadowRadius = 1.0;
        
        [self updateBackground];
        
        [self.layer addSublayer:_contentLayer];
        self.layer.masksToBounds = YES;
    }
    return self;
}

- (void)layoutSubviews
{
    _contentLayer.frame = CGRectInset(self.bounds, 5.0, 0.5);
    //_contentLayer.frame = CGRectMake(5.0, 1.0, self.bounds.size.width - 10.0, self.bounds.size.height - 1.0);
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    [self updateBackground];
}

- (void)updateBackground
{
    if (_selected)
        _contentLayer.backgroundColor = [[UIColor colorWithHue:230.0 / 360.0 saturation:0.2 brightness:1.0 alpha:1.0] CGColor];
    else
        _contentLayer.backgroundColor = [[UIColor whiteColor] CGColor];
}

@end
