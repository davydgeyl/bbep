//
//  JSActionCellBackgroundView.h
//  BBEP
//
//  Created by Davyd Geyl on 18/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSActionCellBackgroundView : UIView

@property (nonatomic, assign) BOOL selected;

@end
