//
//  JSMapViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 14/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSViewController.h"

@class JSMapView;

@interface JSMapViewController : JSViewController

@property (nonatomic, strong) JSMapView* mapView;
@property (nonatomic, strong) UIImage* mapImage;

@end
