//
//  JSLabel.m
//  BBEP
//
//  Created by Davyd Geyl on 17/09/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSLabel.h"

@implementation JSLabel

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    CGFloat width = CGRectGetWidth(self.bounds);
    if (self.preferredMaxLayoutWidth != width) {
        self.preferredMaxLayoutWidth = width;
    }
}

@end
