//
//  JSDashboardButton.h
//  BBEP
//
//  Created by Davyd Geyl on 19/02/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSDashboardButton : UIButton

@property (nonatomic, assign) IBInspectable NSUInteger buttonIndex;

@end
