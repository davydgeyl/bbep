//
//  JSShadowFoldLayer.m
//  BBEP
//
//  Created by Davyd Geyl on 26/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSShadowFoldLayer.h"

@implementation JSShadowFoldLayer

- (void)setExpansion:(CGFloat)expansion
{
    _expansion = MIN(expansion, 1.0);
    [self setNeedsDisplay];
}

- (void)setFoldType:(JSShadowFoldType)foldType
{
    _foldType = foldType;
    [self setNeedsDisplay];
}

- (void)drawInContext:(CGContextRef)ctx
{
    CGFloat shadowBlur = 5.0 * _expansion;
    CGFloat shadowOffset = 3.0 * _expansion;

    CGFloat shadowPathHeight = 8.0 * _expansion;
    CGPoint shadowPathOrigin = { 0, -shadowPathHeight };
    CGFloat flatness = shadowPathHeight * 0.15;

    CGSize boundsSize = self.bounds.size;

    if (_foldType == JSShadowFoldBottom)
    {
        CGContextScaleCTM(ctx, 1.0, -1.0);
        CGContextTranslateCTM(ctx, 0, -self.bounds.size.height);
        shadowOffset = -shadowOffset;
    }

    CGContextMoveToPoint(ctx, shadowPathOrigin.x, shadowPathOrigin.y);

    CGPoint leftMiddlePoint = CGPointMake(shadowPathOrigin.x + shadowPathHeight / 2.0, shadowPathOrigin.y + shadowPathHeight / 2.0);
    CGPoint leftEdgePoint = CGPointMake(shadowPathOrigin.x + shadowPathHeight, shadowPathOrigin.y + shadowPathHeight);
    CGContextAddCurveToPoint(ctx, leftMiddlePoint.x - flatness, shadowPathOrigin.y + flatness, leftMiddlePoint.x - flatness, shadowPathOrigin.y + flatness, leftMiddlePoint.x, leftMiddlePoint.y);
    CGContextAddCurveToPoint(ctx, leftMiddlePoint.x + flatness, leftEdgePoint.y - flatness, leftMiddlePoint.x + flatness, leftEdgePoint.y - flatness, leftEdgePoint.x, leftEdgePoint.y);

    CGPoint rightEdgePoint = CGPointMake(shadowPathOrigin.x + boundsSize.width - shadowPathHeight, shadowPathOrigin.y + shadowPathHeight);
    CGPoint rightMiddlePoint = CGPointMake(shadowPathOrigin.x + boundsSize.width - shadowPathHeight / 2.0, shadowPathOrigin.y + shadowPathHeight / 2.0);

    CGContextAddLineToPoint(ctx, rightEdgePoint.x, rightEdgePoint.y);
    CGContextAddCurveToPoint(ctx, rightMiddlePoint.x - flatness, rightEdgePoint.y - flatness, rightMiddlePoint.x - flatness, rightEdgePoint.y - flatness, rightMiddlePoint.x, rightMiddlePoint.y);
    CGContextAddCurveToPoint(ctx, rightMiddlePoint.x + flatness, shadowPathOrigin.y + flatness, rightMiddlePoint.x + flatness, shadowPathOrigin.y + flatness, shadowPathOrigin.x + boundsSize.width, shadowPathOrigin.y);
    CGContextClosePath(ctx);

    CGContextSetShadowWithColor(ctx, CGSizeMake(0, shadowOffset), shadowBlur, [UIColor colorWithWhite:0 alpha:0.5].CGColor);
    CGContextFillPath(ctx);
}

@end
