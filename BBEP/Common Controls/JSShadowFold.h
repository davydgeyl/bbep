//
//  JSShadowFold.h
//  BBEP
//
//  Created by Davyd Geyl on 26/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSShadowFoldLayer.h"

@interface JSShadowFold : UIView

@property (nonatomic, assign) JSShadowFoldType foldType;
@property (nonatomic, assign) CGFloat expansion;

@end
