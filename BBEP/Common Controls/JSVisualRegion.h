//
//  JSVisualRegion.h
//  BBEP
//
//  Created by Davyd Geyl on 4/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSVisualRegion : UIView

@property (nonatomic, strong) NSDictionary* regionDictionary;

@end
