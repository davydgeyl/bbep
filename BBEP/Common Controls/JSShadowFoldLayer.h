//
//  JSShadowFoldLayer.h
//  BBEP
//
//  Created by Davyd Geyl on 26/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

typedef enum
{
    JSShadowFoldTop,
    JSShadowFoldBottom
}
JSShadowFoldType;

@interface JSShadowFoldLayer : CALayer

@property (nonatomic, assign) JSShadowFoldType foldType;
@property (nonatomic, assign) CGFloat expansion;

@end
