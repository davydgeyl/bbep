//
//  JSViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 25/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSViewController.h"

@interface JSViewController ()

@end

@implementation JSViewController

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0)
        {
            UIImage* buttonImage = [UIImage imageNamed:@"back_arrow.png"];
            CGRect buttonImageFrame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
            UIButton* button = [[UIButton alloc] initWithFrame:buttonImageFrame];
            [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
            [button addTarget:self.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
            [button setShowsTouchWhenHighlighted:YES];
            UIBarButtonItem* buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];

            self.navigationItem.leftBarButtonItem = buttonItem;
        }
    }
    return self;
}

@end
