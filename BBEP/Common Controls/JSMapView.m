//
//  JSMapView.m
//  BBEP
//
//  Created by Davyd Geyl on 3/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSMapView.h"
#import "JSMapInfoViewController.h"
#import "JSVisualRegion.h"

@interface JSMapView () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UITapGestureRecognizer* tapGestureRecognizer;
@property (nonatomic, strong) NSMutableArray* visualRegions;

@end

@implementation JSMapView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _tapGestureRecognizer = [UITapGestureRecognizer new];
        [_tapGestureRecognizer addTarget:self action:@selector(tap:)];
        [self addGestureRecognizer:_tapGestureRecognizer];
        _tapGestureRecognizer.delegate = self;
        
        _visualRegions = [NSMutableArray new];
    }
    return self;
}

- (void)setMapRegions:(NSArray*)mapRegions
{
    _mapRegions = mapRegions;
    [self reloadData];
}

- (void)reloadData
{
    for (JSVisualRegion* visualRegion in _visualRegions)
        [visualRegion removeFromSuperview];
    
    [_visualRegions removeAllObjects];
    
    for (NSDictionary* region in _mapRegions)
    {
        JSVisualRegion* visualRegion = [JSVisualRegion new];
        visualRegion.regionDictionary = region;
        [_visualRegions addObject:visualRegion];
        [self addSubview:visualRegion];
    }
    
    [self setNeedsLayout];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect bounds = self.bounds;
    
    for (JSVisualRegion* visualRegion in _visualRegions)
    {
        NSDictionary* regionDictionary = visualRegion.regionDictionary;
        
        CGFloat x = [(NSNumber*)[regionDictionary objectForKey:@"x"] floatValue];
        CGFloat y = [(NSNumber*)[regionDictionary objectForKey:@"y"] floatValue];
        CGFloat width = [(NSNumber*)[regionDictionary objectForKey:@"width"] floatValue];
        CGFloat height = [(NSNumber*)[regionDictionary objectForKey:@"height"] floatValue];
        
        visualRegion.frame = CGRectMake(x * bounds.size.width, y * bounds.size.height, width * bounds.size.width, height * bounds.size.height);
    }
}

- (JSVisualRegion*)visualRegionAtPoint:(CGPoint)point
{
    JSVisualRegion* result = nil;
    
    for (JSVisualRegion* visualRegion in _visualRegions)
    {
        if (CGRectContainsPoint(visualRegion.frame, point))
        {
            result = visualRegion;
            break;
        }
    }
    
    return result;
}

#pragma mark - UIGestureRecognizerDelegate

- (void)tap:(UITapGestureRecognizer*)gestureRecognizer
{
    CGPoint point = [gestureRecognizer locationInView:self];
    
    JSVisualRegion* hitVisualRegion = [self visualRegionAtPoint:point];
    
    if (hitVisualRegion != nil)
    {
        NSDictionary* region = hitVisualRegion.regionDictionary;
        [self.delegate mapView:self didSelectRegion:region inRect:hitVisualRegion.frame];
    }
    else
        [self.delegate mapView:self didDetectTapAtPoint:point];
}

@end
