//
//  JSMapInfoHostViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 19/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSViewController.h"

@interface JSMapInfoHostViewController : JSViewController

- (instancetype)initWithContentViewController:(UIViewController*)viewController;

@end
