//
//  JSMenuTableViewCell.h
//  BBEP
//
//  Created by Davyd Geyl on 25/02/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSMenuTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *caption;
@property (nonatomic, strong) UIImage *image;

@end
