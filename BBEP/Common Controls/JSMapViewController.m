//
//  JSMapViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 14/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSMapViewController.h"
#import "JSMapView.h"
#import "JSMapInfoViewController.h"
#import "JSPopoverController.h"
#import "JSMapInfoHostViewController.h"
#import "JSConstants.h"

CGFloat const JSTriptychTouchInfoHeightDefault = 150.0;
CGFloat const JSTriptychTouchInfoWidthDefault = 375.0;
CGFloat const JSMapViewBottomMargin = 20.0;

@interface JSMapViewController () <UIScrollViewDelegate, JSMapViewDelegate>

@property (nonatomic, strong) IBOutlet UIScrollView* scrollView;
@property (nonatomic, strong) UIView* contentView;
@property (nonatomic, strong) UIImageView* imageView;
@property (nonatomic, strong) UIPopoverController* currentPopover;
@property (nonatomic, strong) UITapGestureRecognizer* tapGestureRecognizer;
@property (nonatomic, assign) BOOL isFullView;

@end

@implementation JSMapViewController

- (id)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        _contentView = [UIView new];
        _contentView.backgroundColor = [UIColor blackColor];

        _mapView = [JSMapView new];
        _mapView.delegate = self;
        _mapView.autoresizesSubviews = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

        _imageView = [UIImageView new];
        _imageView.autoresizesSubviews = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

        [_contentView addSubview:_imageView];
        [_contentView addSubview:_mapView];

        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    }
    return self;
}

- (id)init
{
    return [self initWithNibName:@"JSMapViewController" bundle:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_scrollView addSubview:_contentView];
    [self.view addGestureRecognizer:_tapGestureRecognizer];
}

- (void)setMapImage:(UIImage*)mapImage
{
    _mapImage = mapImage;
    _imageView.image = _mapImage;
    [self layoutViews];
}

- (void)layoutViews
{
    _contentView.frame = CGRectMake(0, 0, _mapImage.size.width, _mapImage.size.height);
    _imageView.frame = _contentView.frame;
    _mapView.frame = _contentView.frame;
    _scrollView.contentSize = _imageView.frame.size;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self updateZoomScale];
    [self updateContentInset];

    [_scrollView setZoomScale:_scrollView.minimumZoomScale];
}

- (void)updateZoomScale
{
    CGFloat horizontalScale = _scrollView.bounds.size.width / _imageView.image.size.width;
    [_scrollView setMaximumZoomScale:1.2];
    [_scrollView setMinimumZoomScale:horizontalScale];
}

- (void)updateContentInset
{
    CGFloat verticalInset = (_scrollView.bounds.size.height - _scrollView.contentSize.height) / 2.0;
    if (verticalInset < 0)
        verticalInset = 0;

    [_scrollView setContentInset:UIEdgeInsetsMake(verticalInset, 0, 0, 0)];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self updateZoomScale];

    [_scrollView setZoomScale:_scrollView.minimumZoomScale];
}

#pragma mark - Scrol view delegate

- (void)scrollViewDidZoom:(UIScrollView*)scrollView
{
    [self updateContentInset];
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView*)scrollView
{
    return _contentView;
}

#pragma mark - JSMapViewDelegate

- (void)mapView:(JSMapView*)mapView didSelectRegion:(NSDictionary*)region inRect:(CGRect)rect
{
    JSMapInfoViewController* info = [JSMapInfoViewController new];

    info.titlelabel.text = [region objectForKey:@"title"];
    info.detailsLabel.text = [region objectForKey:@"text"];

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        CGSize bounds = [UIScreen mainScreen].bounds.size;
        info.view.frame = CGRectMake(0, 0, bounds.width, bounds.height);
        [info resizeToFit];

        JSMapInfoHostViewController* mapInfoHostViewController = [[JSMapInfoHostViewController alloc] initWithContentViewController:info];

        if (_isFullView)
            [self.navigationController setNavigationBarHidden:NO animated:YES];
        [self.navigationController pushViewController:mapInfoHostViewController animated:YES];
    }
    else
    {
        [info resizeToFit];
        CGFloat contentHeight = info.detailsLabel.frame.origin.y + info.detailsLabel.frame.size.height + JSMapViewBottomMargin;
        self.currentPopover = [[UIPopoverController alloc] initWithContentViewController:info];
        [_currentPopover setPopoverContentSize:CGSizeMake(JSTriptychTouchInfoWidthDefault, contentHeight)];
        [_currentPopover presentPopoverFromRect:rect inView:mapView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (void)mapView:(JSMapView*)mapView didDetectTapAtPoint:(CGPoint)point
{
    self.isFullView = !_isFullView;
}

- (void)singleTap:(UITapGestureRecognizer*)gestureRecognizer
{
    self.isFullView = !_isFullView;
}

- (void)setIsFullView:(BOOL)isFullView
{
    _isFullView = isFullView;
    [self updateFullView];
}

#pragma mark -

- (void)updateFullView
{
    [self.navigationController setNavigationBarHidden:_isFullView animated:YES];
    [self updateContentInset];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (_isFullView)
        [self updateFullView];
}

@end
