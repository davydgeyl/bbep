//
//  JSMapInfoHostViewController.m
//  BBEP
//
//  Created by Davyd Geyl on 19/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSMapInfoHostViewController.h"
#import "JSConstants.h"

@interface JSMapInfoHostViewController ()

@property (nonatomic, strong) UIViewController* contentViewController;

@end

@implementation JSMapInfoHostViewController

- (instancetype)initWithContentViewController:(UIViewController*)viewController;
{
    self = [super init];
    if (self)
    {
        _contentViewController = viewController;
        
        self.view = [UIView new];
        [self.view addSubview:_contentViewController.view];
    }
    return self;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CGFloat navigationBarHeight = [[JSConstants sharedConstants] titleBarHeightForInterfaceOrientation:self.interfaceOrientation];
    
    _contentViewController.view.frame = CGRectMake(0, navigationBarHeight, self.view.bounds.size.width, self.view.bounds.size.height - navigationBarHeight);
}

@end
