//
//  JSPopoverController.m
//  BBEP
//
//  Created by Davyd Geyl on 15/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSPopoverController.h"
#import "JSPopoverView.h"

@interface JSPopoverController ()

@property (nonatomic, strong) UIViewController* contentViewController;

@end

@implementation JSPopoverController

- (id)initWithContentViewController:(UIViewController*)viewController;
{
    self = [super init];
    if (self)
    {
        _contentViewController = viewController;
        self.view = [JSPopoverView new];
        [self.view addSubview:_contentViewController.view];
    }
    return self;
}

- (void)setPopoverContentSize:(CGSize)size animated:(BOOL)animated
{
    _popoverContentSize = size;
    
    [self.view setFrame:CGRectMake(0, 0, _popoverContentSize.width, _popoverContentSize.height)];
    
    _contentViewController.view.frame = self.view.bounds;
}

- (void)setPopoverContentSize:(CGSize)size
{
    [self setPopoverContentSize:size animated:NO];
}

- (void)presentPopoverFromRect:(CGRect)rect inView:(UIView*)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated
{
    CGRect hostBounds = view.bounds;
    CGFloat bottomGap = hostBounds.size.height - rect.size.height - rect.origin.y;
    
    CGPoint origin = CGPointMake(rect.origin.x + rect.size.width / 2.0, rect.origin.y + rect.size.height / 2.0);
    
    if (_popoverContentSize.height < bottomGap)
    {
        origin.y = rect.origin.y;
        origin.x = MAX(rect.origin.x + rect.size.width / 2.0 - _popoverContentSize.width / 2.0, 0);
    }
    else
    {
        
    }
    
    [self.view setFrame:CGRectMake(origin.x, origin.y, _popoverContentSize.width, _popoverContentSize.height)];
    [view addSubview:self.view];
    [self.view becomeFirstResponder];
}

- (void)dismissPopoverAnimated:(BOOL)animated
{
    
}

@end
