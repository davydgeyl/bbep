//
//  JSShadowFold.m
//  BBEP
//
//  Created by Davyd Geyl on 26/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSShadowFold.h"

@interface JSShadowFold ()
{
    JSShadowFoldLayer* _shadowLayer;
}

@end

@implementation JSShadowFold

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    _shadowLayer = [JSShadowFoldLayer new];
    _shadowLayer.foldType = JSShadowFoldTop;
    _shadowLayer.needsDisplayOnBoundsChange = YES;
    [self.layer addSublayer:_shadowLayer];
    self.layer.masksToBounds = YES;
    self.backgroundColor = [UIColor clearColor];
}

- (void)layoutSublayersOfLayer:(CALayer*)layer
{
    if (layer == self.layer)
    {
        _shadowLayer.frame = self.bounds;
    }
}

- (CGFloat)expansion
{
    return _shadowLayer.expansion;
}

- (void)setExpansion:(CGFloat)expansion
{
    _shadowLayer.expansion = expansion;
}

- (JSShadowFoldType)foldType
{
    return _shadowLayer.foldType;
}

- (void)setFoldType:(JSShadowFoldType)foldType
{
    _shadowLayer.foldType = foldType;
}

@end
