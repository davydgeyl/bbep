//
//  JSPopoverView.h
//  BBEP
//
//  Created by Davyd Geyl on 16/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSPopoverView : UIView

@end
