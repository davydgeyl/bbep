//
//  JSMapView.h
//  BBEP
//
//  Created by Davyd Geyl on 3/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JSMapView;

@protocol JSMapViewDelegate <NSObject>

- (void)mapView:(JSMapView*)mapView didSelectRegion:(NSDictionary*)region inRect:(CGRect)rect;
- (void)mapView:(JSMapView*)mapView didDetectTapAtPoint:(CGPoint)point;

@end

@interface JSMapView : UIView

@property (nonatomic, strong) NSArray* mapRegions;
@property (nonatomic, weak) id<JSMapViewDelegate> delegate;

@end
