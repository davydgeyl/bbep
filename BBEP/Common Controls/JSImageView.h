//
//  JSImageView.h
//  BBEP
//
//  Created by Davyd Geyl on 28/02/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSImageView : UIImageView

@property (nonatomic, assign) CGFloat explicitHeight;

@end
