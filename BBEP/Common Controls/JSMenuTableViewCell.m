//
//  JSMenuTableViewCell.m
//  BBEP
//
//  Created by Davyd Geyl on 25/02/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSMenuTableViewCell.h"
#import "JSImageView.h"

static CGFloat const nonImageGaps = 50.0;

@interface JSMenuTableViewCell ()

@property (nonatomic, strong) IBOutlet JSImageView *menuImageView;

@end

@implementation JSMenuTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
	self = [super initWithCoder:coder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (void)commonInit {
	UIView *backgroundView = [[UIView alloc] initWithFrame:self.bounds];
	backgroundView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.4];
	self.selectedBackgroundView = backgroundView;
}

- (void)setImage:(UIImage *)image {
	self.menuImageView.image = image;
    [self udpateImageViewHeight];
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    [self udpateImageViewHeight];
}

- (void)udpateImageViewHeight
{
    self.menuImageView.explicitHeight = self.menuImageView.image ? self.bounds.size.height - nonImageGaps : 0;
}

@end
