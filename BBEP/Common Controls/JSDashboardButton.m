//
//  JSDashboardButton.m
//  BBEP
//
//  Created by Davyd Geyl on 19/02/2015.
//  Copyright (c) 2015 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSDashboardButton.h"

static CGFloat const imageTopPadding = 30.0;
static CGFloat const imageHeight = 200.0;
static CGFloat const imageToTitleGap = 10.0;
static CGFloat const titleHeight = 44.0;

@interface JSDashboardButton ()

@property (nonatomic, strong) UIColor *originalBackgroundColor;

@end

@implementation JSDashboardButton

- (CGRect)imageRectForContentRect:(CGRect)contentRect {
	CGSize originalImageSize = [self imageForState:UIControlStateNormal].size;
	CGSize imageSize = CGSizeMake(round(originalImageSize.width * imageHeight / originalImageSize.height), imageHeight);

	return CGRectMake(round((contentRect.size.width - imageSize.width) / 2.0), imageTopPadding, imageSize.width, imageSize.height);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect {
	CGRect originalTitleRect = [super titleRectForContentRect:contentRect];
	CGRect imageRect = [self imageRectForContentRect:contentRect];

	return CGRectMake((self.bounds.size.width - originalTitleRect.size.width) / 2.0, imageRect.origin.y + imageRect.size.height + imageToTitleGap, self.bounds.size.width, titleHeight);
}

- (void)setBackgroundColor:(UIColor *)backgroundColor {
	[super setBackgroundColor:backgroundColor];
	self.originalBackgroundColor = backgroundColor;
}

- (void)setHighlighted:(BOOL)highlighted {
	[super setHighlighted:highlighted];
	if (highlighted) {
		super.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.4];
	}
	else {
		super.backgroundColor = self.originalBackgroundColor;
	}
}

@end
