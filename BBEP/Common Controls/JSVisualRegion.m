//
//  JSVisualRegion.m
//  BBEP
//
//  Created by Davyd Geyl on 4/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import "JSVisualRegion.h"

@implementation JSVisualRegion

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        CALayer* layer = self.layer;
        layer.borderWidth = 1.0;
        CGColorSpaceRef genericRGBColorSpace = CGColorSpaceCreateDeviceRGB();
        
        CGFloat comps[] = {1.0, 1.0, 0.9, 0.9};
        CGColorRef color = CGColorCreate(genericRGBColorSpace, comps);
        layer.borderColor = color;
        
        CGColorSpaceRelease(genericRGBColorSpace);
        CGColorRelease(color);
        
        layer.cornerRadius = 8.0;
    }
    return self;
}

@end
