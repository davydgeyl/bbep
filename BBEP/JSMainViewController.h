//
//  JSMainViewController.h
//  BBEP
//
//  Created by Davyd Geyl on 11/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JSShadowFold;

@interface JSMainViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIView* descriptionView;
@property (nonatomic, strong) IBOutlet UITableView* actionsTableView;
@property (nonatomic, strong) IBOutlet UITextView* descriptionTextView;
@property (nonatomic, strong) IBOutlet JSShadowFold* topFold;
@property (nonatomic, strong) IBOutlet JSShadowFold* bottomFold;

- (IBAction)showBuddhaMenu:(id)sender;
- (IBAction)showDharmaMenu:(id)sender;
- (IBAction)showSanghaMenu:(id)sender;

@end
