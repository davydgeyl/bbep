//
//  BBEPTests.m
//  BBEPTests
//
//  Created by Davyd Geyl on 3/03/2014.
//  Copyright (c) 2014 Jettysoft Pty Ltd. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface BBEPTests : XCTestCase

@end

@implementation BBEPTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

- (void)testRand {
    
    BOOL isMaxThere = NO;
    BOOL isMinThere = NO;
    for (int i = 0; i < 1000 && !isMaxThere && !isMinThere; i++) {
        NSUInteger nextCardNumber = arc4random_uniform(52);
        
        if (nextCardNumber == 51) {
            isMaxThere = YES;
        }
        
        if (nextCardNumber == 0) {
            isMinThere = YES;
        }
    }
}


@end
